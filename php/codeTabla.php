<?php
require_once ('DBconnect.php');

$LatLon = $_GET ["latlon"];
$Coordenadas = array ();
$Coordenadas = preg_split ( "/[\s|]+/", $LatLon );
$db = new BaseDatos ();
$strSQL = "";
if ($Coordenadas [2] != "NETOWNERS") {
	$strSQL = "select code from $Coordenadas[2] where abs(latitud - ($Coordenadas[0]))<0.00001 and abs(longitud -($Coordenadas[1]))<0.00001";
} else {
	// $strSQL = "select NE.code from $Coordenadas[2] NE,MVPHNODE PH where NE.PHNODE=PH.CODE AND abs(PH.latitud - ($Coordenadas[0]))<0.00001 and abs(PH.longitud -($Coordenadas[1]))<0.00001";
	$strSQL = "select NE.code from $Coordenadas[2] NE where NE.PHNODE IN (SELECT PH.CODE FROM MVPHNODE PH where abs(PH.latitud - ($Coordenadas[0]))<0.0001 and abs(PH.longitud -($Coordenadas[1]))<0.0001)";
}
// echo $strSQL;
$res = $db->ejecutar ( $strSQL );
if ($res != null) {
	$row = $db->fetch ( $res );
	echo $row [0];
} else {
	if ($Coordenadas [2] == "NETOWNERS") {
		// $strSQL = "select NE.code from $Coordenadas[2] NE,LVPHNODE PH where NE.PHNODE=PH.CODE AND abs(PH.latitud - ($Coordenadas[0]))<0.00001 and abs(PH.longitud -($Coordenadas[1]))<0.00001";
		$strSQL = "select NE.code from $Coordenadas[2] NE where NE.PHNODE IN (SELECT PH.CODE FROM LVPHNODE PH where abs(PH.latitud - ($Coordenadas[0]))<0.0001 and abs(PH.longitud -($Coordenadas[1]))<0.0001)";
		$res = $db->ejecutar ( $strSQL );
		if ($res != null) {
			$row = $db->fetch ( $res );
			echo $row [0];
		} else {
			echo "NADA";
		}
	} else {
		echo "NADA";
	}
}
?>
