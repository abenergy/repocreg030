var base_url = "http://104.196.217.215/visorweb030_des/php/";
// var base_url = "php/";

var map;
var geocoder;
var infowindow ;
var directionsService ;
var directionsDisplay ;
var searchBoxIni ;
var searchBoxFinal ;
var userPerfil;

var MetroX =  0.0000090088  ;//Equivalencia de un metro en grados
var MetroX2 = -0.0000090088 ;//Equivalencia de un metro en grados
var MetroY =  0.0000090410 ;//Equivalencia de un metro en grados
var MetroY2 =  -0.0000090410 ;//Equivalencia de un metro en grados

var pLineasBaja =  new Array();
var pTrafos =  new Array();
var pPostesBaja = new Array();
var pMarkers = new Array();
var pLineas =  new Array();
var pPostes =  new Array();

var myLatLng ;
var drawLatLng ;
var spardTruck;
var chosen = [];
var markers = [];

var toggle_insert = false;
var toggle_buscar = false;
var toggle_layers = false;
var toggle_borrar = false;
var toggle_admin = false;
var toggle_config = false;
var toggle_address = false;
var toggle_route = false;
var toggle_addmat = false;
var toggle_inpmat = false;
var toggle_solicitud = false;
var toggle_inspeccion = false;
var toggle_pqr = false;
var flgMapClick = 0;

var SIZE_TRAFO = 3;
var SIZE_LN_MEDIA = 3;
var SIZE_LN_BAJA = 2;
var SIZE_NF_MEDIA = 3;
var SIZE_NF_BAJA = 2;
var SIZE_CUST;
var SIZE_CMBANK;
var SIZE_SUB;
var SIZE_STREETLG;
var SIZE_SWITCH = 2;
var SIZE_RECLOSER = 2;
var mouselat;
var mouselong;
var blnOpcionMapa = 0;

var trafoGoogle = null;

function Init() {        
    initialize();
    // RequestTrafos();
}

function initSearchAddrGoo()
{
    var inputInfo = document.getElementById('frmBuscaInfoGooAddr');
    searchBox = new google.maps.places.SearchBox(inputInfo);
    listenAddrGoo = map.addListener('bounds_changed', function() 
    {
      searchBox.setBounds(map.getBounds());
    });
    var markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBoxListen = searchBox.addListener('places_changed', function()
    {
      var places = searchBox.getPlaces();

      if (places.length == 0)
      {
        return;
      }

      // Clear out the old markers.
      markers.forEach(function(marker) 
      {
        marker.setMap(null);
      });
      markers = [];

      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function(place) {
        if (!place.geometry) {
          console.log("Returned place contains no geometry");
          return;
        }
        var icon = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        // Create a marker for each place.
        markers.push(new google.maps.Marker({
          map: map,
          icon: icon,
          title: place.name,
          position: place.geometry.location
        }));

        if (place.geometry.viewport) 
        {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } 
        else 
        {
          bounds.extend(place.geometry.location);
        }
      });
      map.fitBounds(bounds);
        toggle_buscar = false;
        var buscaInfoInput = document.getElementById("frmBuscaInfoGooAddr");
        buscaInfoInput.value = null;
        var inputSearchGooAddr = document.getElementById('frmBuscaInfoGooAddr');
        var inputSearch = document.getElementById('frmBuscaInfo');
        inputSearchGooAddr.style.display = 'none';
        inputSearch.style.display = 'inline';
    });
}

function initialize() {    
    var myLatLng;
    var strStyle;
    var canvas;
    var LatZero;
    var LonZero;

    strSQL = "SELECT CODE, VALOR FROM ADMON_VISOR WHERE CODE='LATITUD_INI'";
    LatZero = execute(strSQL);
    
    strSQL = "SELECT CODE, VALOR FROM ADMON_VISOR WHERE CODE='LONGITUD_INI'";
    LonZero = execute(strSQL);

    strSQL = "SELECT CODE, VALOR FROM ADMON_VISOR WHERE CODE='MAP_STYE'";
    strStyle = execute(strSQL);

    var myLatLng = new google.maps.LatLng(Number(LatZero[0].VALOR),Number(LonZero[0].VALOR));

    var mapOptions;
    //1 normal style else..
    //2 dark theme style
    //3 white theme style
    if(strStyle[0].VALOR == "2") {
        mapOptions = {
            zoom: 15,
            center: myLatLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles:[{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]
        };
    } else if(strStyle[0].VALOR == "3") {
       mapOptions = {
            zoom: 15,
            center: myLatLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles:[{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
        }; 
    } else {
       mapOptions = {
            zoom: 15,
            center: myLatLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }; 
    }

    canvas = document.getElementById('map-canvas');
    map = new google.maps.Map(canvas, mapOptions);
    geocoder = new google.maps.Geocoder;
    infowindow = new google.maps.InfoWindow;
    directionsService = new google.maps.DirectionsService();
    directionsDisplay = new google.maps.DirectionsRenderer;
    directionsDisplay.setMap(map);

    //SEARCH BOX GOOGLE....
    var input = document.getElementById('txtroutestart');
    var searchBox = new google.maps.places.SearchBox(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
      searchBox.setBounds(map.getBounds());
    });

    var inputFinal = document.getElementById('txtrouteend');
    var searchBoxfinal = new google.maps.places.SearchBox(inputFinal);

    // Bias the SearchBoxfinal results towards current map's viewport.
    map.addListener('bounds_changed', function() {
      searchBoxfinal.setBounds(map.getBounds());
    });

    google.maps.event.addListener(map, 'click', geolocationevent);
}

function refreshmap() {    
    cargarTamanoElementos();
    //TODO: SOLICITAR LA DIRECCION DEL WEB PARA PONERLAS EN EL MAPA. 
    var LatZero;
    var LonZero;

    strSQL = "SELECT CODE, VALOR FROM ADMON_VISOR WHERE CODE='LATITUD_INI'";
    LatZero = execute(strSQL);
    
    strSQL = "SELECT CODE, VALOR FROM ADMON_VISOR WHERE CODE='LONGITUD_INI'";
    LonZero = execute(strSQL);

    myLatLng = new google.maps.LatLng(LatZero2,LonZero);
    
    var strStyle;
    strSQL = "SELECT CODE, VALOR FROM ADMON_VISOR WHERE CODE='MAP_STYE'";
    strStyle = execute(strSQL);
    var mapOptions;
    //1 normal style else..
    //2 dark theme style
    //3 white theme style
    //strStyle[0].VALOR = "2";
    if(strStyle[0].VALOR == "2")
    {
        mapOptions = {
            zoom: 15,
            center: myLatLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles:[{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]
        };
    }
    else if(strStyle[0].VALOR == "3")
    {
       mapOptions = {
            zoom: 15,
            center: myLatLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles:[{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
        }; 
    }
    else
    {
       mapOptions = {
            zoom: 15,
            center: myLatLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            //styles:[{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#d0e3b4"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"color":"#fbd3da"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#bde6ab"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffe15f"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efd151"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"black"}]},{"featureType":"transit.station.airport","elementType":"geometry.fill","stylers":[{"color":"#cfb2db"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#a2daf2"}]}]
        }; 
    }

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    geocoder = new google.maps.Geocoder;
    infowindow = new google.maps.InfoWindow;
    directionsService = new new google.maps.DirectionsService();
    directionsDisplay = new google.maps.DirectionsRenderer;
    directionsDisplay.setMap(map);
    
    for(var i=0;i<pLineasBaja.length;i++)
    {
        pLineasBaja[i].setMap(map);
    }
    for(var i=0;i<pTrafos.length;i++)
    {
        pTrafos[i].setMap(map);
    }
    for(var i=0;i<pPostesBaja.length;i++)
    {
        pPostesBaja[i].setMap(map);
    }    
}

function geolocationevent(event)
{
    var lat = event.latLng.lat();
    var lng = event.latLng.lng();
    var latlng = {lat: lat, lng: lng};
    
    if(toggle_address){
        geocoder.geocode({'location': latlng}, function(results, status) {
          if (status === 'OK') {
            if (results[1]) {
              map.setZoom(18);
              var marker = new google.maps.Marker({
                position: latlng,
                map: map
              });
              pMarkers.push(marker);
              infowindow.setContent(results[0].formatted_address);
              infowindow.open(map, marker);
            } else {
              window.alert('No se encontraron resultados');
            }
          } else {
            window.alert('Falla del Geolocalizador estado: ' + status);
          }
        });
    }
    if(toggle_route){
        var txtRouteStart = document.getElementById("txtroutestart");
        var txtRouteEnd  = document.getElementById("txtrouteend");
        if(flgMapClick == 0){
            txtRouteStart.value = lat +","+ lng;
            flgMapClick = 1;
        } 
        else if(flgMapClick == 1){
            txtRouteEnd.value = lat +","+ lng;
            flgMapClick = 0;    
        }
    }
}

function displayRoute()
{
    var txtRouteStart = document.getElementById("txtroutestart");
    var txtRouteEnd  = document.getElementById("txtrouteend");
    if(txtRouteStart.value != null && txtRouteStart.value != "" && txtRouteEnd.value != null && txtRouteEnd.value != "" ){
        directionsService.route({
          origin: txtRouteStart.value,
          destination: txtRouteEnd.value,
          travelMode: 'DRIVING'
        }, function(response, status) {
          if (status === 'OK') {
            directionsDisplay.setDirections(response);
            var route = response.routes[0];
            var distancia = "";
            for (var i = 0; i < route.legs.length; i++) {
                  //this works fine and displays properly
                  distancia += route.legs[i].distance.text;
                  //I want to store to this array so that I can sort
             }
            document.getElementById("txtDistancia").value = distancia;
            
          } else {
            window.alert('Direccion no encontrada estado:' + status);
          }
        });
    }
}

function buscarElemento() {     
    if(!toggle_buscar) {
        iniciarBuscar();
        $('select').formSelect();
        toggle_buscar = true;    
    } else {
        toggle_buscar = false;
    }
    
    var modalAlert = document.querySelectorAll('#frmBuscar .elemento')[0];
    var msgAlert = document.querySelectorAll('#frmBuscar .alert')[0];
    var inputSearchGooAddr = document.getElementById('frmBuscaInfoGooAddr');
    var inputSearch = document.getElementById('frmBuscaInfo');
    inputSearchGooAddr.style.display = 'none';
    inputSearch.style.display = 'inline';
    toggleClassByElem(modalAlert, 'invalid', true);
    toggleClassByElem(msgAlert, 'hidden', false);
}

function mapaTematico()
{
    var lightbox = document.getElementById("frmThematicMap");
    var btnMapaTematico = document.getElementById("boton-layers");  
    var frmRoute = document.getElementById("frmRoute");   
    var btnRoute = document.getElementById("boton-route");
    btnRoute.style.border = 'none';
    toggle_route = false;
    if(!toggle_layers){
        lightbox.style.visibility = 'visible';
        btnMapaTematico.style.border='2px solid #3399ff';
        toggle_layers = true;    
        iniciarTheMap();
    }
    else 
    {
        lightbox.style.visibility = 'hidden';
        btnMapaTematico.style.border='none';
        toggle_layers = false;
    }
}

function CargarBaja(tparent)
{
    var LinBaja = document.getElementById('cBaja');
    var PostesBaja = document.getElementById('cPosteB');
}

function CentrarMapa(code)
{
    //obtnener coordenadas del alimentador y centrar el mapa en dicha lat y long.
     var xmlhttp;
       xmlhttp = new XMLHttpRequest();          
       xmlhttp.onreadystatechange=function()
        {               
            if (xmlhttp.readyState==4 && xmlhttp.status==200)                
            {                                        
                var res= xmlhttp.responseText;  
                var data = JSON.parse(res);   
                if(data != null)
                {
                    if(map != null && data[0].lat != null && data[0].lon != null)
                    {
                        map.setCenter(new google.maps.LatLng(data[0].lat, data[0].lon));
                    }    
                }
            }
        }            
        xmlhttp.open("GET",base_url+"FeederLatLong.php?code="+code,true);
        xmlhttp.send();  
}

function CambiarTrafos()
{
    ResetRed();
    RequestTrafos();
}
 
//De aqui en adelante las solicitudes por tipo de elemento
function RequestTrafos(reqValue)
{
    showLoader(true);

    var xmlhttp;
    xmlhttp = new XMLHttpRequest();     
    xmlhttp.onreadystatechange=function()
    {          
        if (xmlhttp.readyState===4 && xmlhttp.status==200)                
        {
            CargarTrafos(xmlhttp.responseText,null);

            if (reqValue) {
                var trafo = searchCodeTrafo(reqValue);
                RequestRedBTCodigo(trafo);
            }
        }
    }            
    xmlhttp.open("GET",base_url+"Trafos.php",true);
    xmlhttp.send();
}

function searchCodeTrafo(code) {
    var trafo = null;
    
    for (i=0;i<pTrafos.length;i++) {
        if (pTrafos[i].spCode == code) trafo = pTrafos[i];
    }

    return trafo;
}
    
function RequestLineasB(Trafo, col)
{
    var xmlhttp;
    xmlhttp = new XMLHttpRequest();          
    xmlhttp.onreadystatechange=function()
     {               
         if (xmlhttp.readyState==4 && xmlhttp.status==200)                
         {                                        
             CargarLineasB(xmlhttp.responseText, col, Trafo);                   
         }
     }            
     xmlhttp.open("GET",base_url+"LineasBaja.php?Parents="+Trafo,true);
     xmlhttp.send();  
}

function RequestPostesB(Trafo, col)
{           
   var xmlhttp;
   xmlhttp = new XMLHttpRequest();          
   xmlhttp.onreadystatechange=function()
    {               
        if (xmlhttp.readyState==4 && xmlhttp.status==200)                
        {                                        
            CargarPostesB(xmlhttp.responseText, col, Trafo);                   
        }
    }            
    xmlhttp.open("GET",base_url+"postesBaja.php?Parents="+Trafo,true);
    xmlhttp.send();   
}

function RequestLineas(Trafo)
{
    var xmlhttp;
    xmlhttp = new XMLHttpRequest();          
    xmlhttp.onreadystatechange=function()
     {               
         if (xmlhttp.readyState==4 && xmlhttp.status==200)                
         {                                        
             CargarLineas(xmlhttp.responseText);                   
         }
     }            
     xmlhttp.open("GET",base_url+"Lineas.php?Trafo="+Trafo,true);
     xmlhttp.send();  
}

function RequestPostes(Trafo)
{           
   var xmlhttp;
   xmlhttp = new XMLHttpRequest();          
   xmlhttp.onreadystatechange=function()
    {               
        if (xmlhttp.readyState==4 && xmlhttp.status==200)                
        {                                        
            CargarPostes(xmlhttp.responseText);                   
        }
    }            
    xmlhttp.open("GET",base_url+"postes.php?Trafo="+Trafo,true);
    xmlhttp.send();   
}

function RequestFeeder(Trafo)
{           
   var xmlhttp;
   xmlhttp = new XMLHttpRequest();          
   xmlhttp.onreadystatechange=function()
    {               
        if (xmlhttp.readyState==4 && xmlhttp.status==200)                
        {                                        
            CargarFeeder(xmlhttp.responseText);                   
        }
    }            
    xmlhttp.open("GET",base_url+"feeder.php?trafo="+Trafo,true);
    xmlhttp.send();   
}

function ToolTipTrafo(event)
{
    var vertices = this.getPath();
    var location = vertices.getAt(0);
    trafoGoogle = this;
    if(this.spCode !="")
    {
       var Trafo = this.spCode;
       var Info;
       
       var i;
       var a;
       var xmlDOC = loadXMLDoc("CFG/TooltipConf.xml");
       while(xmlDOC == null)
       {
            var xmlDOC = loadXMLDoc("CFG/TooltipConf.xml");           
       }
       var capas = xmlDOC.getElementsByTagName("element");
       strSQL = "SELECT ";
              
       for(i=0;i<capas.length;i++)
       {
           if(capas[i].attributes[0].value == "TRANSFOR")
            {
               for(a=1;a<capas[i].childElementCount;a++)
                {
                   strSQL += capas[i].children[a].attributes[0].value;
                   if(a < capas[i].childElementCount -1)
                      strSQL += ","
                }
               strSQL += " FROM "+capas[i].children[0].textContent+ " WHERE CODE='"+Trafo+"'";
               Info = execute(strSQL);
               
               contentString = '<div id="bodyContent">'
                   var tm;
                for(a=1;a<capas[i].childElementCount;a++)
                {
                    tm = capas[i].children[a].attributes[0].value;
                    contentString += "<b>"+capas[i].children[a].textContent+": </b>"+Info[0][""+tm+""]+"<br>";
                }
                contentString += '</div>';
                contentString += '<div id="botonesTrafo">';
                contentString += '<table cellspacing="20" cellpadding="2">';
                contentString += '<tr>'; 
                contentString += '<td><a href="javascript:void(0)" onclick="RequestRedMTCodigo()" class="btnGenerico">Red Media</a></td>';
                contentString += '<td>';
                contentString += '</td>';
                contentString += '<td><a href="javascript:void(0)" onclick="RequestRedBTCodigo(trafoGoogle)" class="btnGenerico">Red Baja</a></td>';
                contentString += '<td>';
                contentString += '</td>';
                contentString += '<td>';
                contentString += '</td> ';
                contentString += '</tr>';
                contentString += '</table> '; 
                contentString += '</div>';
                //contentString += '<script src="JS/carguestooltip.js"></script>';
               break;
            }
       }

       var infowindow = new google.maps.InfoWindow({
            content: contentString,
            position: location
        });
       infowindow.open(map);
   }
}

function RequestRedBTCodigo(elTrafo)
{
    var xmlhttp;
    var loc;
    var Trafo = elTrafo.spCode;

    if(!elTrafo.hasBaja)
    {
        RequestLineasB(Trafo, elTrafo.Color);
        RequestPostesB(Trafo, elTrafo.Color);
        elTrafo.hasBaja = true;
    }
    else
    {
        BorrarLineasB(Trafo);
        BorrarPostesB(Trafo);
        trafoGoogle.hasBaja = false;
    }
}

function RequestRedMTCodigo()
{
   var xmlhttp;
   var loc;
   var Trafo = trafoGoogle.spCode;
   BorrarLineas();
   BorrarPostes();
   //RequestFeeder(Trafo);
   RequestLineas(Trafo);
   RequestPostes(Trafo);
}

function RequestRedBT(event)
{
   var vertices = this.getPath();
   var location = vertices.getAt(0);
   var xmlhttp;
   var loc;
   var Trafo = this.spCode;
   if(!this.hasBaja)
   {
        RequestLineasB(Trafo, this.Color);
        RequestPostesB(Trafo, this.Color);
        this.hasBaja = true;
   }
   else
   {
        BorrarLineasB(Trafo);
        BorrarPostesB(Trafo);
        this.hasBaja = false;
   }
}

function BorrarFeeder()
{
}

function BorrarLineas()
{
    for(var i=0;i<pLineas.length;i++)
    {
        pLineas[i].setMap(null);
    }
    //delete pLineas;
    pLineas = new Array();
}

function BorrarPostes()
{
    for(var i=0;i<pPostes.length;i++)
    {
        pPostes[i].setMap(null);
    }
    //delete pPostes;
    pPostes = new Array();
}

function BorrarLineasB(Trafo)
{
    for(var i=0;i<pLineasBaja.length;i++)
    {
       if(pLineasBaja[i].tparent == Trafo)
       {
         pLineasBaja[i].setMap(null);
         //pLineasBaja.splice(i,1);
       }
    }            
}

function BorrarPostesB(Trafo)
{
    for(var i=0;i<pPostesBaja.length;i++)
    {
       if(pPostesBaja[i].tparent == Trafo)
       {
         pPostesBaja[i].setMap(null);
         //pPostesBaja.splice(i,1);
       }
    }
}

function CargarLineasB(LineasRaw, col, tparent)
{
    var Lineas = JSON.parse(LineasRaw);                 
    var Color;
    var Linea;
    var Path;
    for(var i=0;i<Lineas.length;i++)
    {    
        Color = col;
        Path = [
                new google.maps.LatLng(Lineas[i].lat1,Lineas[i].lon1),
                new google.maps.LatLng(Lineas[i].lat2,Lineas[i].lon2)
                ];               
        Linea =  new google.maps.Polyline({
                path: Path,
                strokeColor: Color,
                strokeOpacity: 1.0,
                strokeWeight: SIZE_LN_BAJA                       
                });
        //google.maps.event.addListener(Linea, 'click', ToolTipLineaB);       
        Linea.spCode = Lineas[i].code;
        Linea.tparent = tparent;
        pLineasBaja.push(Linea);
        Linea.setMap(map);
    }            
}

function CargarPostesB(PostesRaw,col, tparent)
{
    var Postes = JSON.parse(PostesRaw);
    var Color;
    var Poste; 
    for(var i=0;i<Postes.length;i++)
    {       
        Color = col;        
        var populationOptions = {
                path: google.maps.SymbolPath.CIRCLE,
                strokeColor: Color,
                strokeOpacity: 1.0,
                strokeWeight: 1,
                fillColor: Color,
                fillOpacity: 0.8,
                map: map,
                center: new google.maps.LatLng(Postes[i].lat1,Postes[i].lon1),
                radius: SIZE_NF_BAJA
                };
        Poste = new google.maps.Circle(populationOptions);
        google.maps.event.addListener(Poste, 'click', PosteBajaEventMan);        
        Poste.spCode = Postes[i].code;
        Poste.tparent = tparent;
        pPostesBaja.push(Poste);
        Poste.setMap(map);
    }  
}

function CargarLineas(LineasRaw)
{
    var Lineas = JSON.parse(LineasRaw);                 
    var color;
    var Linea;
    var Path;
    var tipoMapa = document.getElementById('Radio_Tipo_Tematico');
    color = "#00FF00";
    for(var i=0;i<Lineas.length;i++)
    {    
        if(tipoMapa[0].checked)
        {
            //Potencia generada. Se espera ya en porcentaje
            if(Lineas[i].pg >= 0 && Lineas[i].pg <= 9)  color = "#00FF00";
            if(Lineas[i].pg > 9 && Lineas[i].pg <= 12)  color = "#FFFF00";
            if(Lineas[i].pg > 12 && Lineas[i].pg <= 15)  color = "#FF8000";
            if(Lineas[i].pg > 15 && Lineas[i].pg <= 100)  color = "#FF0000";
        }
        if(tipoMapa[1].checked)
        {
            //Energia entregada. Se espera ya en porcentaje
            if(Lineas[i].eg >= 0 && Lineas[i].eg <= 30)  color = "#00FF00";
            if(Lineas[i].eg > 30 && Lineas[i].eg <= 40)  color = "#FFFF00";
            if(Lineas[i].eg > 40 && Lineas[i].eg <= 50)  color = "#FF8000";
            if(Lineas[i].eg > 50 && Lineas[i].eg <= 100)  color = "#FF0000";
        }
        Path = [
                new google.maps.LatLng(Lineas[i].lat1,Lineas[i].lon1),
                new google.maps.LatLng(Lineas[i].lat2,Lineas[i].lon2)
                ];               
        Linea =  new google.maps.Polyline({
                path: Path,
                strokeColor: color,
                strokeOpacity: 1.0,
                strokeWeight: SIZE_LN_MEDIA                      
                });
        //google.maps.event.addListener(Linea, 'click', ToolTipLinea);       
        Linea.spCode = Lineas[i].code;
        pLineas.push(Linea);
        Linea.setMap(map);
    }            
}

function CargarPostes(PostesRaw, col)
{
    var Postes = JSON.parse(PostesRaw);
    var color;
    var Poste;
    var tipoMapa = document.getElementById('Radio_Tipo_Tematico');
    color = "#00FF00";
    for(var i=0;i<Postes.length;i++)
    {       
        if(tipoMapa[0].checked)
        {
            //Potencia generada. Se espera ya en porcentaje
            if(Postes[i].pg >= 0 && Postes[i].pg <= 9)  color = "#00FF00";
            if(Postes[i].pg > 9 && Postes[i].pg <= 12)  color = "#FFFF00";
            if(Postes[i].pg > 12 && Postes[i].pg <= 15)  color = "#FF8000";
            if(Postes[i].pg > 15 && Postes[i].pg <= 100)  color = "#FF0000";
        }
        if(tipoMapa[1].checked)
        {
            //Energia entregada. Se espera ya en porcentaje
            if(Postes[i].eg >= 0 && Postes[i].eg <= 30)  color = "#00FF00";
            if(Postes[i].eg > 30 && Postes[i].eg <= 40)  color = "#FFFF00";
            if(Postes[i].eg > 40 && Postes[i].eg <= 50)  color = "#FF8000";
            if(Postes[i].eg > 50 && Postes[i].eg <= 100)  color = "#FF0000";
        }       
        var populationOptions = {
                path: google.maps.SymbolPath.CIRCLE,
                strokeColor: color,
                strokeOpacity: 1.0,
                strokeWeight: 1,
                fillColor: color,
                fillOpacity: 0.8,
                map: map,
                center: new google.maps.LatLng(Postes[i].lat1,Postes[i].lon1),
                radius: SIZE_NF_MEDIA
                };
        Poste = new google.maps.Circle(populationOptions);
        google.maps.event.addListener(Poste, 'click', PosteEventMan);        
        Poste.spCode = Postes[i].code;
        pPostes.push(Poste);
        Poste.setMap(map);
    }  
}

function CargarTrafos(TrafosRaw)
{
    CambiaTheMap2();//Refresca el tema
    var tipoMapa = document.getElementById('Radio_Tipo_Tematico');
 
    var Trafos = JSON.parse(TrafosRaw);
    var color;
    color = "#00FF00";
    for(var i=0;i<Trafos.length;i++)
    {       
        var Trafo;
        var Path;
        if(tipoMapa[0].checked)
        {
            //Potencia generada. Se espera ya en porcentaje
            if(Trafos[i].pg > 0 && Trafos[i].pg <= 9)  color = "#00FF00";
            if(Trafos[i].pg > 9 && Trafos[i].pg <= 12)  color = "#FFFF00";
            if(Trafos[i].pg > 12 && Trafos[i].pg <= 15)  color = "#FF8000";
            if(Trafos[i].pg > 15 && Trafos[i].pg <= 100)  color = "#FF0000";
        }
        if(tipoMapa[1].checked)
        {
            //Energia entregada. Se espera ya en porcentaje
            if(Trafos[i].eg > 0 && Trafos[i].eg <= 30)  color = "#00FF00";
            if(Trafos[i].eg > 30 && Trafos[i].eg <= 40)  color = "#FFFF00";
            if(Trafos[i].eg > 40 && Trafos[i].eg <= 50)  color = "#FF8000";
            if(Trafos[i].eg > 50 && Trafos[i].eg <= 100)  color = "#FF0000";
        }
        if(tipoMapa[2].checked)
        {
            //Energia entregada. Se espera ya en porcentaje
            if(Trafos[i].egv > 0 && Trafos[i].egv <= 30)  color = "#00FF00";
            if(Trafos[i].egv > 30 && Trafos[i].egv <= 40)  color = "#FFFF00";
            if(Trafos[i].egv > 40 && Trafos[i].egv <= 50)  color = "#FF8000";
            if(Trafos[i].egv > 50 && Trafos[i].egv <= 100)  color = "#FF0000";
        }

        var trafoMetroX = MetroX * SIZE_TRAFO;
        var trafoMetroX2 = MetroX2 * SIZE_TRAFO;
        var trafoMetroY = MetroY * SIZE_TRAFO;
        var LatitudY;
        var LongitudX;

        LatitudY = Trafos[i].lat;
        LongitudX = Trafos[i].lon;

        Path = [new google.maps.LatLng(LatitudY,LongitudX),                        
                new google.maps.LatLng(LatitudY-4*trafoMetroY,LongitudX-2*trafoMetroX),
                new google.maps.LatLng(LatitudY-4*trafoMetroY,LongitudX-2*trafoMetroX2)
                ];
  
        Trafo = new google.maps.Polygon({
            paths: Path,
            strokeColor: color,
            strokeOpacity: 1.0,
            strokeWeight: 2,
            fillColor: color,
            fillOpacity: 0.5                   
        });

        google.maps.event.addListener(Trafo, 'dblclick', RequestRedBT);
        google.maps.event.addListener(Trafo, 'click', ToolTipTrafo);
        Trafo.spCode = Trafos[i].code;
        Trafo.Color = color;
        Trafo.hasBaja = false;
        pTrafos.push(Trafo);
        Trafo.setMap(map);
    }

    showLoader(false);
}

function BorrarCapasTematicas()
{
    var sel = document.getElementById('objListaGrupos');
    if(sel.childElementCount > 0)
    {
        $('#objListaGrupos').empty();
    }
}

function ToolTipLineaB(event)
{
   var vertices = this.getPath();
   var location =  new google.maps.LatLng(event.latLng.lat(),event.latLng.lng());
   var xmlhttp;
   var loc;
   var strSQL;
   if(this.spCode !="")
   {
       var lineaB = this.spCode;
       var Info;
       
       var i;
       var a;
       if(userPerfil){
            var xmlDOC = loadXMLDoc("CFG/TooltipConf."+userPerfil+".xml");
        }else{
            var xmlDOC = loadXMLDoc("CFG/TooltipConf.xml");
        }
       var capas = xmlDOC.getElementsByTagName("element");
       strSQL = "SELECT ";
       for(i=0;i<capas.length;i++)
       {
           if(capas[i].attributes[0].value == "LVLINSEC")
            {
               for(a=1;a<capas[i].childElementCount;a++)
                {
                   strSQL += capas[i].children[a].attributes[0].value;
                   if(a < capas[i].childElementCount -1)
                      strSQL += ","
                }
               strSQL += " FROM "+capas[i].children[0].textContent+ " WHERE CODE='"+lineaB+"'";
               Info = execute(strSQL);
               
               contentString = '<div id="bodyContent">'
                   var tm;
                for(a=1;a<capas[i].childElementCount;a++)
                {
                    tm = capas[i].children[a].attributes[0].value;
                    contentString += "<b>"+capas[i].children[a].textContent+": </b>"+Info[0][""+tm+""]+"<br>";
                }
                contentString += '</div>';
               break;
            }
       }
       var infowindow = new google.maps.InfoWindow({
            content: contentString,
            position: location
        });
       infowindow.open(map);
   }
}

function ResetRed()
{
    for(var i=pLineasBaja.length-1;i>-1;i--)
    {
        pLineasBaja[i].setMap(null);
        pLineasBaja.pop();
    }
    for(var i=pTrafos.length-1;i>-1;i--)
    {
        pTrafos[i].setMap(null);
        pTrafos.pop();
    }    
    for(var i=pPostesBaja.length-1;i>-1;i--)
    {
        pPostesBaja[i].setMap(null);
        pPostesBaja.pop();
    }
    BorrarLineas();
    BorrarPostes();
}

function CambiaTipoBusca(tabla) {
    
    if(tabla == 'GOOGLE')
    {
        var inputSearchGooAddr = document.getElementById('frmBuscaInfoGooAddr');
        var inputSearch = document.getElementById('frmBuscaInfo');
        inputSearchGooAddr.style.display = 'inline';
        inputSearch.style.display = 'none';
        var searchBox = new google.maps.places.SearchBox(inputSearchGooAddr);
    }
    else
    {
        var inputSearchGooAddr = document.getElementById('frmBuscaInfoGooAddr');
        var inputSearch = document.getElementById('frmBuscaInfo');
        inputSearchGooAddr.style.display = 'none';
        inputSearch.style.display = 'inline';
        var xmlhttp = new XMLHttpRequest();          
        xmlhttp.onreadystatechange=function()
        {
            if (xmlhttp.readyState==4 && xmlhttp.status==200)                
            {
                var campo = JSON.parse(xmlhttp.responseText);

                if(userPerfil){
                    var xmlDOC = loadXMLDoc("CFG/FieldsConf."+userPerfil+".xml");
                }else{
                    var xmlDOC = loadXMLDoc("CFG/FieldsConf.xml");
                }
                while(xmlDOC == null){
                    if(userPerfil){
                        var xmlDOC = loadXMLDoc("CFG/FieldsConf."+userPerfil+".xml");
                    }else{
                        var xmlDOC = loadXMLDoc("CFG/FieldsConf.xml");
                    }
                }
                var elementos = xmlDOC.getElementsByTagName("element");
                //var elementos = xmlDOC.getElementsByTagName("field");
            
                for(var a=0;a<elementos.length;a++) 
                 {
                    if(elementos[a].attributes[1].value == tabla)
                    {
                        for(var i = 0; i < campo.length; i++)
                        {
                             for(var b=0;b<elementos[a].children.length;b++)
                            {
                                if(campo[i].code.toUpperCase() == elementos[a].children[b].attributes[0].value && (elementos[a].children[b].attributes[0].value == 'CODE' || elementos[a].children[b].attributes[0].value == 'code' || elementos[a].children[b].attributes[0].value == 'INVNUMBER' || elementos[a].children[b].attributes[0].value == 'invnumber'))
                                {
                                     var opt = document.createElement('option');
                                     opt.text = elementos[a].children[b].textContent;//opt.innerHTML = campo[i].code;
                                     opt.value = campo[i].code + ":" + campo[i].tipo;
                                     break;
                                }
                            }
                        }
                        break;
                    }
                 }
                /*for(var i = 0; i < campo.length; i++) 
                 {
                     var opt = document.createElement('option');
                     opt.innerHTML = campo[i].code;
                     opt.value = campo[i].code + ":" + campo[i].tipo;
                     sel.appendChild(opt);
                 }*/
            }
         }            
         xmlhttp.open("GET",base_url+"lstCampos.php?TABLA="+tabla+"&TIPO=3",true);
         xmlhttp.send();
    }

    var toggle = true;
    if (tabla == 'null') toggle = false;
    var elementoMsg = document.querySelectorAll('#frmBuscar .elemento')[0];
    toggleClassByElem(elementoMsg, 'invalid', toggle);
    
}

function CambiaTheMapTbl(valor)
{
    var sel = document.getElementById('frmTheMap1');
    var length = sel.options.length;
    for (var i = 1; i < length; i++) {
        sel.options[1] = null;
    }

    BorrarCapasTematicas();

    var xmlhttp = new XMLHttpRequest();          
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)                
        {
            var campo = JSON.parse(xmlhttp.responseText);
            if(userPerfil){
                var xmlDOC = loadXMLDoc("CFG/FieldsConf."+userPerfil+".xml");
            }else{
                var xmlDOC = loadXMLDoc("CFG/FieldsConf.xml");
            }
            while(xmlDOC == null){
                if(userPerfil){
                    var xmlDOC = loadXMLDoc("CFG/FieldsConf."+userPerfil+".xml");
                }else{
                    var xmlDOC = loadXMLDoc("CFG/FieldsConf.xml");
                }
            }
            var elementos = xmlDOC.getElementsByTagName("element");
            //var elementos = xmlDOC.getElementsByTagName("field");
            for(var a=0;a<elementos.length;a++)
            {
                if(elementos[a].attributes[1].value == valor)
                {
                    for(var i = 0; i < campo.length; i++)
                    {
                        for(var b=0;b<elementos[a].children.length;b++)
                        {
                            if((campo[i].code.toUpperCase() == elementos[a].children[b].attributes[0].value) && (elementos[a].children[b].attributes[1].value == "si" || elementos[a].children[b].attributes[1].value == "SI"))
                            {
                                 var opt = document.createElement('option');
                                 opt.text = elementos[a].children[b].textContent;//opt.innerHTML = campo[i].code;
                                 opt.value = campo[i].code + ":" + campo[i].tipo;
                                 sel.appendChild(opt);
                                 break;
                            }
                        }
                    }
                    break;
                }
            }
        /* for(var i = 0; i < campo.length; i++) 
        {
            var opt = document.createElement('option');
            opt.innerHTML = campo[i].code;
            opt.value = campo[i].code + ":" + campo[i].tipo;
            sel.appendChild(opt);
        }*/
        }
     }            
     xmlhttp.open("GET",base_url+"lstCampos.php?TABLA="+valor+"&TIPO=3",true);
     xmlhttp.send();
}

function CambiaTheMap2()
{
    BorrarCapasTematicas();
   
    var sel = document.getElementById('objListaGrupos');
    var tipoMapa = document.getElementById('Radio_Tipo_Tematico');

    var min;
    var max;
    var color;


    if(tipoMapa[0].checked)
    {
        //Potencia generada. Se espera ya en porcentaje       
        for(var i = 0; i < 4; i++) 
        {
            var opt = document.createElement('li');
            var span = document.createElement('span');
            if(i == 0) 
            {
                min = 0;
                max = 9;
                color = "#00FF00";
            }
            if(i == 1) 
            {
                min = 9;
                max = 12;
                color = "#FFFF00";
            }
            if(i == 2) 
            {
                min = 12;
                max = 15;
                color = "#FF8000";
            }
            if(i == 3) 
            {
                min = 15;
                max = 100;
                color = "#FF0000";
            }
            opt.innerText = min+"% - "+ max+"%";
            // opt.value = color;
            opt.setAttribute('id','objGrupo'+i);
            span.style.background = color;
            opt.appendChild(span);
            sel.appendChild(opt);
        }
    }
    if(tipoMapa[1].checked || tipoMapa[2].checked)
    {
        //Energia entregada. Se espera ya en porcentaje
        for(var i = 0; i < 4; i++) 
        {
            var opt = document.createElement('li');
            var span = document.createElement('span');
            if(i == 0) 
            {
                min = 0;
                max = 30;
                color = "#00FF00";
            }
            if(i == 1) 
            {
                min = 30;
                max = 40;
                color = "#FFFF00";
            }
            if(i == 2) 
            {
                min = 40;
                max = 50;
                color = "#FF8000";
            }
            if(i == 3) 
            {
                min = 50;
                max = 100;
                color = "#FF0000";
            }
            opt.innerText = min+"% - "+ max+"%";
            // opt.value = color;
            opt.setAttribute('id','objGrupo'+i);
            span.style.background = color;
            opt.appendChild(span);
            sel.appendChild(opt);
        }
    }
}

function execute(strSQL)
{
    var xmlhttp;
    xmlhttp = new XMLHttpRequest();          
    xmlhttp.open("POST",base_url+"consulta.php?strSQL="+strSQL+"&token=12345",false);
    xmlhttp.send(); 
    return  JSON.parse(xmlhttp.responseText);     
}

function decimalToHex(d, padding)
{
    var hex = Number(d).toString(16);
    padding = typeof (padding) === "undefined" || padding === null ? padding = 2 : padding;
    while (hex.length < padding)
    {
         hex = "0" + hex;
    }
    return hex;
}  

function DarFases(a, b, c)
{
    var res = 0;
    if(a)
        res += 1;
    if(b)
        res += 2;
    if(c)
        res += 4;
    return res;
}

function loadXMLDoc(filename)
{
    if (window.XMLHttpRequest)
    {
        xhttp=new XMLHttpRequest();
    }
    else // code for IE5 and IE6
    {
        xhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    xhttp.open("GET",filename,false);
    xhttp.send();
    return xhttp.responseXML;
}

function PosteEventMan(event)//ACAXXXX
{
    var centro = this.getCenter();
    var Objposte = this.spCode;
    var Info;

    if(!toggle_solicitud)
    {        
        var i;
        var a; 
        var xmlDOC = loadXMLDoc("CFG/TooltipConf.xml");
        
        while(xmlDOC == null)
        {
            var xmlDOC = loadXMLDoc("CFG/TooltipConf.xml");
        }

        var capas = xmlDOC.getElementsByTagName("element");
        strSQL = "SELECT ";


        for(i=0;i<capas.length;i++)
        {
           if(capas[i].attributes[0].value == "MVPHNODE")
            {
               for(a=1;a<capas[i].childElementCount;a++)
                {
                   strSQL += capas[i].children[a].attributes[0].value;
                   if(a < capas[i].childElementCount -1)
                      strSQL += ","
                }
               strSQL += " FROM "+capas[i].children[0].textContent+ " WHERE CODE='"+Objposte+"'";
               Info = execute(strSQL);
               
               contentString = '<div id="bodyContent">'
                   var tm;
                for(a=1;a<capas[i].childElementCount;a++)
                {
                    tm = capas[i].children[a].attributes[0].value;
                    contentString += "<b>"+capas[i].children[a].textContent+": </b>"+Info[0][""+tm+""]+"<br>";
                }
               break;
            }
        }
        var infowindow = new google.maps.InfoWindow({
            content: contentString,
            position: centro
        });
        infowindow.open(map);
    }
    else
    {        
        var strSQL;
        strSQL = "SELECT FPARENT, '-' AS TPARENT, KVNOM, ADDRESS, IF_1PH_0, IF_3PH_0 FROM MVELNODE, MVELNDRS WHERE MVELNDRS.SRCCODE = MVELNODE.CODE AND MVELNODE.CODE = '"+Objposte+"'";
        Info = execute(strSQL);
        document.getElementById("txtNodo").value = Objposte;
        document.getElementById("txtCircuito").value = Info[0]["FPARENT"];
        document.getElementById("txtTransformador").value = Info[0]["TPARENT"];
        document.getElementById("txtKvNom").value = Info[0]["KVNOM"];
        // document.getElementById("txtAddress").value = Info[0]["ADDRESS"];
        document.getElementById("txtMonoCorto").value = Math.round(Info[0]["IF_1PH_0"]*100.0)/100;
        document.getElementById("txtIcorto").value = Math.round(Info[0]["IF_3PH_0"]*100.0)/100;
    }
}

function PosteBajaEventMan(event)//ACAXXX
{
    showLoader(true);
    console.log('nodo ', this.spCode);
    toggle_solicitud = true;

    var centro = this.getCenter();
    var Objposte = this.spCode;
    var Info;
    var Infokva;

    if(!toggle_solicitud)
    {        
        var i;
        var a; 
        var xmlDOC = loadXMLDoc("CFG/TooltipConf.xml");
        
        while(xmlDOC == null)
        {
            var xmlDOC = loadXMLDoc("CFG/TooltipConf.xml");
        }

        var capas = xmlDOC.getElementsByTagName("element");
        strSQL = "SELECT ";

        for(i=0;i<capas.length;i++)
        {
           if(capas[i].attributes[0].value == "LVPHNODE")
            {
               for(a=1;a<capas[i].childElementCount;a++)
                {
                   strSQL += capas[i].children[a].attributes[0].value;
                   if(a < capas[i].childElementCount -1)
                      strSQL += ","
                }
               strSQL += " FROM "+capas[i].children[0].textContent+ " WHERE CODE='"+Objposte+"'";
               Info = execute(strSQL);
               
               contentString = '<div id="bodyContent">'
                   var tm;
                for(a=1;a<capas[i].childElementCount;a++)
                {
                    tm = capas[i].children[a].attributes[0].value;
                    contentString += "<b>"+capas[i].children[a].textContent+": </b>"+Info[0][""+tm+""]+"<br>";
                }
               break;
            }
        }
        var infowindow = new google.maps.InfoWindow({
            content: contentString,
            position: centro
        });
        showLoader(false);
        infowindow.open(map);
    }
    else
    {
        strSQL = "SELECT 'nada' AS NADA, COUNT(*) AS CANTIDAD FROM LVLINSEC WHERE TPARENT = '"+this.tparent+"' AND ACO = 0";
        Info = execute(strSQL);
        if(Info[0][""] > 0)
        {
            var strSQL;
            strSQL = "SELECT FPARENT, TPARENT, KVNOM, ADDRESS, IF_1PH_0, IF_3PH_0 FROM LVELNODE, LVELNDRS WHERE LVELNDRS.SRCCODE = LVELNODE.CODE AND LVELNODE.CODE = '"+Objposte+"'";
            Info = execute(strSQL);

            strSQL = "SELECT KVA, TRANSFOR.CODE AS CODE FROM TRANSFOR, TRFTYPES WHERE TRFTYPES.CODE = TRANSFOR.TRFTYPE AND TRANSFOR.CODE = '"+Info[0]["TPARENT"]+"'";
            Infokva = execute(strSQL);

            document.getElementById("txtNodo").value = Objposte;
            document.getElementById("txtCircuito").value = Info[0]["FPARENT"];
            document.getElementById("txtTransformador").value = Info[0]["TPARENT"];
            document.getElementById("txtPotencia").value = Infokva[0]["KVA"];
            document.getElementById("txtKvNom").value = Info[0]["KVNOM"]*1000;
            // document.getElementById("txtAddress").value = Info[0]["ADDRESS"];

            document.getElementById("txtVoltajeActual").value = "-";   
            document.getElementById("txtVoltajeNuevo").value = "-";   
            document.getElementById("txtRegulacionActual").value = "-";  
            document.getElementById("txtRegulacionNueva").value = "-";  
            document.getElementById("txtCaidaTensionActual").value = "-";
            document.getElementById("txtCaidaTensionNueva").value = "-";
            document.getElementById("txtMonoCorto").value = Math.round(Info[0]["IF_1PH_0"]*10000)/10000;
            document.getElementById("txtIcorto").value = Math.round(Info[0]["IF_3PH_0"]*10000)/10000;
        }
        else
        {
            var TextoAlert = document.getElementById("lblAlert");
            TextoAlert.innerHTML = 'Transformador sin red. Por favor contacatar a la empresa de en energia';
        }

        showLoader(false);
        frmSolicitudInit(Info[0]["KVNOM"]*1000);
    }
    
}
 
function cerrarAlert()
{
    var lightbox = document.getElementById("frmAlert");
    lightbox.style.visibility = 'hidden';
}

function iniciarBuscar()
{
    var xmlDOC = loadXMLDoc("CFG/LayersConf.xml");
    
    while(xmlDOC == null)
    {
        var xmlDOC = loadXMLDoc("CFG/LayersConf.xml");
    }
    
    var capas = xmlDOC.getElementsByTagName("Layer");
    var layersDiv = document.getElementById('layers');
    
    var sel = document.getElementById('frmBuscaTipo');
    var length = sel.options.length;
    for (var i = 1; i < length; i++)
    {
        sel.options[1] = null;
    }
    
    var opt = document.createElement('option');
    opt.text = "Dirección";
    opt.value = "GOOGLE";
    sel.appendChild(opt);
    var xmlDOC = loadXMLDoc("CFG/FieldsConf.xml");
    while(xmlDOC == null)
    {
        var xmlDOC = loadXMLDoc("CFG/FieldsConf.xml");
    }
    var elementos = xmlDOC.getElementsByTagName("element");
    
    for(var i=0;i<capas.length;i++)
    {
        for(var a=0;a<elementos.length;a++)
        {
            if(capas[i].children[4].textContent == elementos[a].attributes[0].value)
            {
                var capa = document.getElementById(capas[i].children[1].textContent);
               
                sel = document.getElementById('frmBuscaTipo');
                var opt = document.createElement('option');
                opt.text = capas[i].children[0].textContent;
                opt.value = elementos[a].attributes[1].value;
                sel.appendChild(opt);
                break;
            }
        }
    }

    //MOSTRAR 
     var xmlhttp = new XMLHttpRequest();          
     xmlhttp.onreadystatechange=function()
     {
         if (xmlhttp.readyState==4 && xmlhttp.status==200)                
         {
             var campo = JSON.parse(xmlhttp.responseText);
             
             elementos = xmlDOC.getElementsByTagName("field");
             
             for(var i = 0; i < campo.length; i++) 
             {
                 for(var a=0;a<elementos.length;a++)
                {
                     if(campo[i].code == elementos[a].attributes[0].value)
                    {
                         var opt = document.createElement('option');
                         opt.text = elementos[a].textContent;//opt.innerHTML = campo[i].code;
                         opt.value = campo[i].code + ":" + campo[i].tipo;
                         entro = true;
                         break;
                    }
                }
             }
         }
      }            
      xmlhttp.open("GET",base_url+"lstCampos.php?TIPO=3",true);
      xmlhttp.send();
}

// Adds a marker to the map and push to the array.
function addMarker(location, title) {
    var marker = new google.maps.Marker({
       position: location,
       map: map,
       title: title
    });
    markers.push(marker);
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }
}

function BuscarEl(frmBuscaTipo, frmBuscaCampo, frmBuscaInfo) {
    //Logica para buscar en Google
    if(frmBuscaTipo == "null") {
        var elementoDiv = document.querySelectorAll('#frmBuscar .elemento')[0];
        toggleClassByElem(elementoDiv, 'invalid', false);

        return;
    }

    //Logica para buscar en Google
    if(frmBuscaTipo == "GOOGLE") {
        //TODO BUSCA DIRECCION EN GOOGLE 
    }

    var xmlhttp = new XMLHttpRequest();          
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
            var campo = JSON.parse(xmlhttp.responseText);

            if(campo.length > 0 && campo != "[]") {
                clearMarkers();
                if(campo.length == 1) {
                    toggle_buscar = false;

                    var lat = campo[0].lat;
                    var lon = campo[0].lon;
                    var center = new google.maps.LatLng(lat, lon);
                    map.panTo(center);
                    map.setZoom(18);
                    var buscaInfoInput = document.getElementById("frmBuscaInfo");
                    buscaInfoInput.value = null;

                    // trafoG
                    addMarker(center, frmBuscaInfo);
                    RequestTrafos(frmBuscaInfo);
                } else {
                    var sel = document.getElementById('objLista');
                    var length = sel.options.length;
                    for (var i = 0; i < length; i++) {
                        sel.options[0] = null;
                    }
                    for(var i = 0; i < campo.length; i++) {
                        var opt = document.createElement('option');
                        opt.text = campo[i].code;
                        opt.value = campo[i].lat+";"+campo[i].lon;
                        sel.appendChild(opt);
                    }
                    
                    var lightbox = document.getElementById("frmLista");
                    lightbox.style.visibility = 'visible';
                }
                
                closeModal('frmBuscar');
            } else {
                var msgAlert = document.querySelectorAll('#frmBuscar .alert')[0];
                toggleClassByElem(msgAlert, 'hidden', true);
            }
        }
     }
     xmlhttp.open("GET",base_url+"buscar.php?TABLA="+frmBuscaTipo+"&CAMPO="+frmBuscaCampo+"&INFO="+frmBuscaInfo,true);
     xmlhttp.send();
}

function rgb2hex(rgb)
{
 rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
 return (rgb && rgb.length === 4) ? "#" +
  ("0" + parseInt(rgb[1],10).toString(16)).slice(-2) +
  ("0" + parseInt(rgb[2],10).toString(16)).slice(-2) +
  ("0" + parseInt(rgb[3],10).toString(16)).slice(-2) : '';
}

function ObtenerColor(index)
{
    var colorArray = ["#FF0000", "#0000FF","#FFFF00","#00FF00" , "#FF5E00", "#470063"];
    if(colorArray.length >= index){
        return colorArray[index];
    }
    else{
        return '#'+Math.floor(Math.random()*16777215).toString(16);
    }
    
}

function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

function rgbToHex(r, g, b) {
    return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}

function cargarTamanoElementos()
{
    var xmlhttp;
       xmlhttp = new XMLHttpRequest();          
       xmlhttp.onreadystatechange=function()
        {               
            if (xmlhttp.readyState==4 && xmlhttp.status==200)                
            {                                        
                 
                var configuracion = xmlhttp.responseText;
                configuracion = JSON.parse(configuracion);
                var mapstyle ;
                //carga la configuracion
                if(configuracion != null || configuracion != ""){

                    for (i=0;i<configuracion.length;i++){
                        if(configuracion[i].CODE == "SIZE_SUB"){
                            SIZE_SUB = configuracion[i].VALOR;
                        }
                        if(configuracion[i].CODE == "SIZE_LN_MEDIA"){
                            SIZE_LN_MEDIA = configuracion[i].VALOR;
                        }
                        if(configuracion[i].CODE == "SIZE_LN_BAJA"){
                            SIZE_LN_BAJA = configuracion[i].VALOR;
                        }
                        if(configuracion[i].CODE == "SIZE_NF_MEDIA"){
                            SIZE_NF_MEDIA = parseInt(configuracion[i].VALOR);
                        }
                        if(configuracion[i].CODE == "SIZE_NF_BAJA"){
                            SIZE_NF_BAJA = parseInt(configuracion[i].VALOR);
                        }
                        if(configuracion[i].CODE == "SIZE_TRAFO"){
                            SIZE_TRAFO = configuracion[i].VALOR;
                        }
                        if(configuracion[i].CODE == "SIZE_CUST"){
                            SIZE_CUST = configuracion[i].VALOR;
                        }
                        if(configuracion[i].CODE == "SIZE_CMBANK"){
                            SIZE_CMBANK = configuracion[i].VALOR;
                        }
                        if(configuracion[i].CODE == "SIZE_SWITCH"){
                            SIZE_SWITCH = parseInt(configuracion[i].VALOR);
                        }//SIZE_STREETLG
                        if(configuracion[i].CODE == "SIZE_STRLIGHT")
                        {
                            SIZE_STREETLG = parseInt(configuracion[i].VALOR);
                        }
                        if(configuracion[i].CODE == "SIZE_RECLOSER")
                        {
                            SIZE_RECLOSER = parseInt(configuracion[i].VALOR);
                        }
                    }
                }                   
            }
        }            
        xmlhttp.open("GET",base_url+"CargaConfig.php",true);
        xmlhttp.send();
}

function botonmapa_resaltar(opcion)
{
    //return;
    var inserta = document.getElementById('boton-insert');
    var elimina = document.getElementById('boton-delete');

    if (opcion==1) {
        if(!toggle_insert){
            blnOpcionMapa = 1;
            inserta.style.border='none';
            elimina.style.border='2px solid #3399ff';
            toggle_insert = true;    
        }
        else 
        {
            blnOpcionMapa = 0;
            inserta.style.border='none';
            elimina.style.border='none';
            toggle_insert = false;
        }
        
    }
    else if (opcion==2) {
        if(!toggle_borrar){
            blnOpcionMapa = 2;
            elimina.style.border='none';
            inserta.style.border='2px solid #3399ff';
            toggle_borrar = true;
        }
        else{
            blnOpcionMapa = 0;
            elimina.style.border='none';
             inserta.style.border='none';
            toggle_borrar = false;

        }
        
    }
}

function exitconntent()
{
    var conntent = document.getElementById("content");
    conntent.style.visibility = 'none';
}

function addresslookup()
{
    var btnConfig = document.getElementById('boton-address');
    if(!toggle_address)
    {
        btnConfig.style.border='2px solid #3399ff';
        toggle_address = true;    
    }
    else 
    {
        for(var i=pMarkers.length-1;i>-1;i--)
        {
            pMarkers[i].setMap(null);
            pMarkers.splice(i, 1);
        }
        btnConfig.style.border='none';
        toggle_address = false;  
    }
}

function route()
{
    var btnRoute = document.getElementById('boton-route');
    var frmRoute = document.getElementById("frmRoute"); 
    var frmThematicMap = document.getElementById("frmThematicMap");   
    //var btnThematicMap = document.getElementById("boton-layers");
    //frmThematicMap.style.visibility = 'hidden';
    //btnThematicMap.style.border = 'none';
    toggle_layers = false;
    if(!toggle_route)
    {
        if(directionsDisplay == null)
        {
            directionsDisplay = new google.maps.DirectionsRenderer;
            directionsService = new google.maps.DirectionsService();
            directionsDisplay.setMap(map);
        }   
        frmRoute.style.visibility = 'visible';
        btnRoute.style.border='2px solid #3399ff';
        toggle_route = true;    
    }
    else 
    {
        if(directionsDisplay != null && directionsService != null)
        {
            directionsDisplay.setMap(null);
            directionsDisplay = null; 
            directionsService = null;
            //Limpia los campos de Coordenadas Inicial y Final
            document.getElementById("txtroutestart").value = "";
            document.getElementById("txtrouteend").value = ""; 
            document.getElementById("txtDistancia").value = "";  
        }
        frmRoute.style.visibility = 'hidden';
        btnRoute.style.border='none';
        toggle_route = false;    
    }
}

window.onload = function() {
    
};