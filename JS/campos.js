function GuardarCampos(Pid,Pnombre, Ptabla){
    if(!validarForm(Pid,Pnombre, Ptabla)){
        return false;
    }
    var xmlhttp;
    var url    = "php/web_CamposIA.php";
    var params = "id="+Pid+"&nombre="+Pnombre+"&tabla="+Ptabla;
    xmlhttp = new XMLHttpRequest();   
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200){                       
            var strresultado = xmlhttp.responseText; 
            if(strresultado == "1")
                return true;
            else 
                return false;                  
        }
    };
    xmlhttp.open("GET", url+"?"+params,true);
    xmlhttp.send();
}

function validarForm(Pid,Pnombre, Ptabla){
    var objInfo =  document.getElementById("msnError");
    objInfo.style.color = "#BABABA";
    var flgError = false;
    objInfo.innerHTML = "";
    //Obtiene todos los combos y se resaltan de color azul para senalar campos necesarios. 
    var objnombre = document.getElementById("txtNombre");
    if(Pnombre == ""){
         objInfo.innerHTML += "El campo  nombre es necesario<br>";
         objnombre.style.border = '2px solid #3399ff';
         flgError = true;
    }
    else{
        objnombre.style.border = 'none';
    }
    if(flgError){
        return false;
    }
    return true;
}

function EliminarCampo(Pid){
	var xmlhttp;
    var xmlhttp;
    var url    = "php/web_CamposEliminar.php";
    var params = "id=" + Pid;
    xmlhttp = new XMLHttpRequest();          
    xmlhttp.onreadystatechange=function(){               
        if (xmlhttp.readyState==4 && xmlhttp.status==200){                       
        	//se obtiene el resultado.                 
            var strresultado = xmlhttp.responseText; 
            if(strresultado == "1")
            	return true;
            else 
            	return false;                  
        }
    };            
    xmlhttp.open("GET", url +"?"+params,true);
    xmlhttp.send();
}

function SeleccionUsuario(nombre, cargo, login, md5, perfil){
	var xmlhttp;
    xmlhttp = new XMLHttpRequest();          
    xmlhttp.onreadystatechange=function(){               
        if (xmlhttp.readyState==4 && xmlhttp.status==200)                
        {                       
        	//se obtiene el resultado.                 
            var strresultado = xmlhttp.responseText; 
            //recorre y llena la el selec de usuario...                
        }
    };            
    xmlhttp.open("GET","php/usu_select.php",true);
    xmlhttp.send();
}