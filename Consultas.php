<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Reportes</title>
<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/consultas.css">
<link href="frameworks/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="frameworks/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="frameworks/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="frameworks/sweetalert2/sweetalert2.min.css">
<link href="frameworks/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<link href="frameworks/datatables/jquery.dataTables.min.css" rel="stylesheet">
<link href="frameworks/datatables/buttons.dataTables.min.css" rel="stylesheet">

<script src="frameworks/jquery/jquery-1.12.4.js"></script>
<script src="JS/consultas.js"></script>
<script src="frameworks/popper/popper.min.js"></script>
<script src="frameworks/bootstrap/js/bootstrap.min.js"></script>
<script src="frameworks/sweetalert2/sweetalert2.min.js"></script>
<script type="text/javascript" src="frameworks/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="frameworks/bootstrap-datetimepicker/js/locale/bootstrap-datetimepicker.es.js" charset="UTF-8"></script>
<script src="frameworks/chart.js/Chart.min.js"></script>
<script src="frameworks/datatables/jquery.dataTables.min.js"></script>
<script src="frameworks/datatables/dataTables.bootstrap4.js"></script>
<script src="frameworks/datatables/dataTables.buttons.min.js"></script>
<script src="frameworks/datatables/jszip.min.js"></script>
<script src="frameworks/datatables/pdfmake.min.js"></script>
<script src="frameworks/datatables/vfs_fonts.js"></script>
<script src="frameworks/datatables/buttons.html5.min.js"></script>
<script src="frameworks/jquery-loading-overlay-master/loadingoverlay.min.js"></script>

</head>
<body>
<div id="reportes">
	<div class="combo">
		<h2 class="h2_title">Generador de Reportes SPARD &reg;  </h2><select id="LstReportes"
			onchange="FormularioDinamico(this.value)">
			<option value="">Seleccionar</option>
		</select>
	</div >
</div>
<div id="logo"><img src="images/spard.jpg"></div>
<br>
<div id="id01"></div>
<div id="Graficos"></div>
</body>
</html>
