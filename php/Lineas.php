<?php
require_once ('DBconnect.php');

$Parents = $_GET ["Trafo"];
$db = new BaseDatos ();
$ParentsArray = array ();
$ParentsArray = preg_split ( "/[\s|]+/", $Parents );
$pCode = "";
if (count ( $ParentsArray ) == 1)
{
    $Fparent = $ParentsArray [0];
    $token = "";
}
if (count ( $ParentsArray ) > 1)
{
    $Fparent = $ParentsArray [0];
    $token = $ParentsArray[1];
}

$strSQL = "select mvlinsec.code as code, lat1, lon1, lat2, lon2, pg, eg from mvlinsec, feeders where feeders.code = fparent and fparent = (select fparent from transfor where code = '$Parents')";

$Lineas = array ();
$res = $db->ejecutar ( $strSQL );
while ( $row = $db->fetch ( $res ) ) {
	$Lineas [] = array (
			'code' => $row [0],
			'lat1' => str_replace ( ",", ".", $row [1] ),
			'lon1' => str_replace ( ",", ".", $row [2] ),
			'lat2' => str_replace ( ",", ".", $row [3] ),
			'lon2' => str_replace ( ",", ".", $row [4] ),
			'pg' => str_replace ( ",", ".", $row [5] ),
			'eg' => str_replace ( ",", ".", $row [6] )
	);
}

echo json_encode ( $Lineas );
?>
