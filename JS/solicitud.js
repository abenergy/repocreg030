function EnviarSolicitud() {
	var IsMedia = false;
	var Media;
	if(document.getElementById("txtTransformador").value == "-") IsMedia = true;
	if(IsMedia)
		Media = 1;
	else
		Media = 0;

	var URL;
	var Nodo = document.getElementById("txtNodo");
    var TipoGen = document.getElementById("cmbTipo");  
    var Descriptio = document.getElementById("txtObservaciones");   
    var Name = document.getElementById("txtUserName");
	var Cap = document.getElementById("txtGenCap");  
	var Cuenta = document.getElementById("txtCuenta");
	var Fp = document.getElementById("txtPowerFactor");
	var Email = document.getElementById("txtCorreo");
	var xmlhttp = new XMLHttpRequest();
	
	xmlhttp.onreadystatechange=function()
	{               
	    if (xmlhttp.readyState==4 && xmlhttp.status==200)                
	    {       
			var MensajeSolicitud;
	    	var Res = JSON.parse(xmlhttp.responseText);
			toggle_solicitud = false;   
	        if(Res[2][1] != true) 
	        {
	        	if(Res[1][1] == false)
	        	{
	        		MensajeSolicitud = 'Su numero de Solicitud es: '+Res[0][1];
	        	}  
	        	else
	        	{
	        		MensajeSolicitud = 'Su numero de Solicitud es: '+Res[0][1]+'\nADVERTENCIA!. Ya existe una solicitud pendiente en el nodo';
	        	}   	
	        }
	      	else
        	{
        		if(Res[3][1] == 1) MensajeSolicitud = 'Ya hay una solicitud pendiente al mismo nodo\ndel mismo correo';
        		if(Res[3][1] == 2) MensajeSolicitud = 'Porcentaje de potencia instalada supera el 15% de la capacidad del alimentador';
        		if(Res[3][1] == 3) MensajeSolicitud = 'Porcentaje de energia generada horaria supera el 50% de la capacidad del alimentador';
        	}  
			document.getElementById("mensajeSolicitud").innerHTML= MensajeSolicitud;			
	    }
		
	}   
	URL = base_url+"enviarsolicitud.php?CUENTA="+Cuenta.value+"&NODO="+Nodo.value+"&CAP="+Cap.value+"&DESCRIPTIO="+Descriptio.value+"&NOMBRE="+Name.value+"&TIPO="+TipoGen.value+"&PF="+Fp.value+"&EMAIL="+Email.value+"&MEDIA="+Media; 
	xmlhttp.open("POST",URL,true);
	xmlhttp.send();

	// Reset form inputs and messages
	document.getElementById("solicitud_form").reset();
	resetForm();
}

function RealizarSimulacion()
{
	var IsMedia = false;
	if(document.getElementById("txtTransformador").value == "-") IsMedia = true;
	var Nodo = document.getElementById("txtNodo");
	var Fp = document.getElementById("txtPowerFactor");
	var Cap = document.getElementById("txtGenCap"); 

	xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange=function()
	{               
	    if (xmlhttp.readyState==4 && xmlhttp.status==200)                
	    {                                        
	        ShowResults(xmlhttp.responseText,null);                   
	    }
	}    
	if(!IsMedia)
   		xmlhttp.open("POST",base_url+"analisissensibilidad.php?NODO="+Nodo.value+"&CAP="+Cap.value+"&PF="+Fp.value,true);
   	else
   		xmlhttp.open("POST",base_url+"analisissensibilidadmedia.php?NODO="+Nodo.value+"&CAP="+Cap.value+"&PF="+Fp.value,true);
   xmlhttp.send();
}

function InspeccionarPqr(NumeroSolicitud)
{
	/*var Url = "Consultas.php?id="+NumeroSolicitud;
	window.open(Url, '_blank');*/

	var strSQL = "";
	xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function()
	{               
	    if (xmlhttp.readyState==4 && xmlhttp.status==200)                
	    {                                        
	        ShowSolicitud(xmlhttp.responseText,null);                   
	    }
	}
	
    xmlhttp.open("POST",base_url+"pqr.php?id="+NumeroSolicitud,false);
    xmlhttp.send();
}

function InspeccionarSolicitud(NumeroSolicitud) {
	var numSolicitud = document.getElementById('txtNumeroSolicitud');
	
	if (numSolicitud.classList.contains('invalid') == true || numSolicitud.value == "" ) {
		var msgAlert = document.querySelectorAll('#frmInspeccion .alert')[0];
		toggleClassByElem(msgAlert, 'hidden', false);
	} else {
		var strSQL = "";
		xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {               
			if (xmlhttp.readyState==4 && xmlhttp.status==200) {                                       
				ShowSolicitud(xmlhttp.responseText,null);
				closeModal('frmInspeccion');
				
				var msgAlert = document.getElementById('frmRealAlert');
				toggleClassByElem(msgAlert, 'open', false);
			}
		}
		
		xmlhttp.open("POST",base_url+"solicitudes.php?id="+NumeroSolicitud,false);
		xmlhttp.send();
	}
	
}

function ShowSolicitud(ResRaw) {
	var Res = JSON.parse(xmlhttp.responseText);	    		   
	var TextoAlert = document.getElementById("lblAlert");
	var Estado;
	if(Res.length == 0 || Res.length == 1) {
		Estado = "Solicitud no encontrada";
		TextoAlert.innerHTML = 'Su solicitud esta en estado\n'+Estado;
	} else {
		var Tabla = transformJson2html(Res);	
		TextoAlert.innerHTML = "";
        TextoAlert.innerHTML+= Tabla;
	}
}

function transformJson2html(Res)
{
	var Tabla = "<table>";
	for(i=0;i<Res.length;i++)
	{
		Tabla+="<tr>";
		for(j=0;j<Res[i].length;j++)
		{
			Tabla+="<td>";
			Tabla+=Res[i][j];
			Tabla+="</td>";
		}
		Tabla+="</tr>";
	}
	Tabla += "</table>";
	return Tabla;
}

function ShowResults(ResRaw)
{
    var Res = JSON.parse(ResRaw); 
    var VoltajeActual = document.getElementById("txtVoltajeActual");   
    var VoltajeNuevo = document.getElementById("txtVoltajeNuevo");   
    var RegulacionActual = document.getElementById("txtRegulacionActual");  
    var RegulacionNueva = document.getElementById("txtRegulacionNueva");  
	var CaidaActual = document.getElementById("txtCaidaTensionActual");
	var CaidaNueva = document.getElementById("txtCaidaTensionNueva");
	VoltajeNuevo.value = Math.round(Res[8][1]*1000.0)/1000;
	RegulacionNueva.value = Math.round(Res[9][1]*100.0)/100;
	CaidaNueva.value = Math.round(Res[10][1]*100.0)/100;
	VoltajeActual.value = Math.round(Res[7][1]*1000.0)/1000;
	RegulacionActual.value = Math.round(Res[11][1]*100.0)/100;
	CaidaActual.value = Math.round(Res[12][1]*100.0)/100;
}