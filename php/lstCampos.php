<?php
require_once ('DBconnect.php');

if(empty($_GET ["TABLA"]) == false){
	$tabla = $_GET ["TABLA"];
}else{
	$tabla = null;
}
if(empty($_GET ["TIPO"]) == false){
	$tipo = $_GET ["TIPO"];
}else{
	$tipo = null;
}
$db = new BaseDatos ();

//error_log ( print_r ( $tipo.$tabla, TRUE ), 0 );

$datos = array ();
$strSQL = "";
if($db->dbtype == 1)
{
    if($tabla != '' || $tabla != null)
    {
    	if($tipo == 1 || $tipo == "1")
    		$strSQL = "SELECT column_name, DATA_TYPE FROM USER_TAB_COLUMNS WHERE table_name = '$tabla' AND DATA_TYPE='NUMBER' ORDER BY column_name ASC";
    	else if($tipo == 2 || $tipo == "2")
    		$strSQL = "SELECT column_name, DATA_TYPE FROM USER_TAB_COLUMNS WHERE table_name = '$tabla' AND DATA_TYPE='VARCHAR2' ORDER BY column_name ASC";
    	else
    		$strSQL = "SELECT column_name, DATA_TYPE FROM USER_TAB_COLUMNS WHERE table_name = '$tabla' AND DATA_TYPE IN('NUMBER','VARCHAR2') ORDER BY column_name ASC";
    }
    else
    { 
    	if($tipo == 1 || $tipo == "1")
    		$strSQL = "SELECT DISTINCT column_name, DATA_TYPE FROM USER_TAB_COLUMNS WHERE table_name IN('MVPHNODE','MVLINSEC','TRANSFOR','LVLINSEC','CUSTMETR','CMBANKS','STREETLG','LVPHNODE','NETOWNERS','SWITCHES','RECLOSER') AND DATA_TYPE='NUMBER' ORDER BY column_name ASC";
    	else if($tipo == 2 || $tipo == "2")
    		$strSQL = "SELECT DISTINCT column_name, DATA_TYPE FROM USER_TAB_COLUMNS WHERE table_name IN('MVPHNODE','MVLINSEC','TRANSFOR','LVLINSEC','CUSTMETR','CMBANKS','STREETLG','LVPHNODE','NETOWNERS','SWITCHES','RECLOSER') AND DATA_TYPE='VARCHAR2' ORDER BY column_name ASC";
    	else
    		$strSQL = "SELECT DISTINCT column_name, DATA_TYPE FROM USER_TAB_COLUMNS WHERE table_name IN('MVPHNODE','MVLINSEC','TRANSFOR','LVLINSEC','CUSTMETR','CMBANKS','STREETLG','LVPHNODE','NETOWNERS','SWITCHES','RECLOSER') AND DATA_TYPE IN('NUMBER','VARCHAR2') ORDER BY column_name ASC";
    }
}
else if($db->dbtype == 0)
{
    if($tabla != '' || $tabla != null)
    {
        if($tipo == 1 || $tipo == "1")
            $strSQL = "SELECT column_name,udt_name FROM information_schema.columns WHERE upper(table_name) = '$tabla' AND udt_name='numeric' ORDER BY column_name ASC";
        else if($tipo == 2 || $tipo == "2")
            $strSQL = "SELECT column_name,udt_name FROM information_schema.columns WHERE upper(table_name) = '$tabla' AND udt_name='varchar' ORDER BY column_name ASC";
        else
            $strSQL = "SELECT column_name,udt_name FROM information_schema.columns WHERE upper(table_name) = '$tabla' AND udt_name IN('numeric','varchar') ORDER BY column_name ASC";
    }
    else
    {
        if($tipo == 1 || $tipo == "1")
            $strSQL = "SELECT column_name,udt_name FROM information_schema.columns WHERE upper(table_name) IN('MVPHNODE','MVLINSEC','TRANSFOR','LVLINSEC','CUSTMETR','CMBANKS','STREETLG','LVPHNODE','NETOWNERS','SWITCHES','RECLOSER') AND udt_name='numeric' ORDER BY column_name ASC";
        else if($tipo == 2 || $tipo == "2")
            $strSQL = "SELECT column_name,udt_name FROM information_schema.columns WHERE upper(table_name) IN('MVPHNODE','MVLINSEC','TRANSFOR','LVLINSEC','CUSTMETR','CMBANKS','STREETLG','LVPHNODE','NETOWNERS','SWITCHES','RECLOSER') AND udt_name='varchar' ORDER BY column_name ASC";
        else
            $strSQL = "SELECT column_name,udt_name FROM information_schema.columns WHERE upper(table_name) IN('MVPHNODE','MVLINSEC','TRANSFOR','LVLINSEC','CUSTMETR','CMBANKS','STREETLG','LVPHNODE','NETOWNERS','SWITCHES','RECLOSER') AND udt_name IN('numeric','varchar') ORDER BY column_name ASC";
    }
}
$res = $db->ejecutar ( $strSQL );
error_log ( print_r ( $strSQL, TRUE ), 0 );

while ( $row = $db->fetch ( $res ) ) {
	$datos [] = array (
			'code' => $row [0],
			'tipo' => $row [1]
	);
}
//$db->desconectar ();
echo json_encode ( $datos );

?>
