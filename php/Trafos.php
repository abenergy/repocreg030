<?php
require_once ('DBconnect.php');
$strSQL = "select code, latitud, longitud, energia_generada, potencia_generada, energia_generada_fv from transfor_web";
//error_log(print_r("trafo ".$strSQL, TRUE), 0);
$Lineas = array ();
$db = new BaseDatos ();
$res = $db->ejecutar ( $strSQL );
while ( $row = $db->fetch ( $res ) ) 
{
	$Lineas [] = array (
			'code' => $row [0],
			'lat' => str_replace ( ",", ".", $row [1] ),
			'lon' => str_replace ( ",", ".", $row [2] ),
			'eg' => str_replace ( ",", ".", $row [3] ),
			'pg' => str_replace ( ",", ".", $row [4] )
	);
}

echo json_encode ($Lineas);
?>
