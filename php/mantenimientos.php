<?php
require_once ('DBconnect.php');
$db = new BaseDatos ();

$strSQL = "SELECT MANT_ID AS manid, ORT_ID AS ot, CODE_ELEMENT AS codelem, TYPE_ELEMENT AS typel, DATE_MAN AS fecha, STATE_MAN AS estadom, COMMENTARY AS info FROM maintenance";

$zona = array ();

$res = $db->ejecutar ( $strSQL );
while ( $row = $db->fetch ( $res ) ) {
    array_push($zona,array(
        'manid' => $row[0],
        'ot' => $row[1],
        'codelem' => $row[2],
        'typel' => $row[3],
        'fecha' => $row[4],
        'estadom' => $row[5],
        'info' => $row[6]
    ));
}
echo json_encode ( $zona );
?>