<?php
require_once ('DBconnect.php');

$db = new BaseDatos ();

$strSQL = "select code, latitud, longitud, orientatio, numfeds from srcbuses";
// error_log(print_r($strSQL, TRUE), 0);
$Srcbuses = array ();
$res = $db->ejecutar ( $strSQL );
while ( $row = $db->fetch ( $res ) ) {
	$Srcbuses [] = array (
			'code' => $row [0],
			'lat' => str_replace ( ",", ".", $row [1] ),
			'lon' => str_replace ( ",", ".", $row [2] ),
			'orientatio' => $row [3],
			'numfeds' => $row [4] 
	);
}
echo json_encode ( $Srcbuses );
?>
