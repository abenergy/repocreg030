<?php
require_once ('DBconnect.php');
$db = new BaseDatos ();

$strSQL = "SELECT ZONAID, ZONACOD, ZONANOM FROM ZNASPARD";

$zona = array ();

$res = $db->ejecutar ( $strSQL );
while ( $row = $db->fetch ( $res ) ) {
    array_push($zona,array(
        'znid' => $row[0],
        'zncod' => $row[1],
        'znnom' => $row[2]
    ));
}
echo json_encode ( $zona );

?>