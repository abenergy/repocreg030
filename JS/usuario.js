function GuardarUsuario(Pnombre, Pcargo, Plogin, Pmd5, confirmarmd5, Pperfil){
    if(!validarForm(Pnombre, Pcargo, Plogin, Pmd5, confirmarmd5, Pperfil)){
        return;
    }
    var objInfo =  document.getElementById("msnError");
    objInfo.style.color = "#BABABA";
    var objnombre = document.getElementById("txtNombre");
    var objcargo = document.getElementById("txtCargo");
    var objlogin = document.getElementById("txtLogin");
    var objpsw = document.getElementById("txtPswd");
    var objconfirmacion = document.getElementById("txtConfirmacion");
    objInfo.innerHTML = "";
    var xmlhttp;
    var url    = "php/usu_insert.php";
    var params = "nombre="+Pnombre+"&cargo="+Pcargo+"&login="+Plogin+"&md5="+Pmd5+"&perfil="+Pperfil;
    xmlhttp = new XMLHttpRequest();   
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200){                       
            var strresultado = xmlhttp.responseText; 
            if(strresultado == "1")
            {
                 objInfo.innerHTML = "El usuario fue registrado exitosamente";
                 objnombre.value = "";
                 objcargo.value = "";
                 objlogin.value = "";
                 objpsw.value = "";
                 objconfirmacion.value = "";
                 //guardar en tabla WEB_USR_ZONA
                 var xmlhttp2 = new XMLHttpRequest();
                 xmlhttp2.onreadystatechange = function() {
					 if (xmlhttp2.readyState==4 && xmlhttp2.status==200){
						 var lstUsr = document.getElementById("objLstZnUsr");
						 for(var i = 0; i < lstUsr.options.length; i++){
							 var op = lstUsr.options.item(i);
							 var par = "login="+Plogin+"&znid="+op.value;
							 var xmlhttp1 = new XMLHttpRequest();
							 xmlhttp1.open("GET", "php/addUsrZna.php?"+par,true);
							 xmlhttp1.send();     
						 }
						 cargarZonasDisp();
						 iniciaListaUsados();
						 cargarUsuarios();
						 return true;
					 }
				}
				xmlhttp2.open("GET", "php/delUsrZna.php?login="+Plogin,true);
				xmlhttp2.send();
            }
            else {
                objInfo.innerHTML = "El usuario no fue registrado";
                return false;                  
            }
        }
    };
    xmlhttp.open("GET", url+"?"+params,true);
    xmlhttp.send();
}
function generaLogin(objNombre){
    //TODO VALIDAR QUE EL LOGIN  ESTE DISPONIBLE
    var login = document.getElementById("txtLogin");
    login.value = objNombre.replace(/\s/g, '');
}

function cargarUsuarios(){
	var sel = document.getElementById('sltBuscar');
    while(sel.length != 0){
        for(var i=0; i < sel.length; i++){
            //sel.removeChild( sel.options[i]); 
            sel.options[i] = null;
        }
    }
    var Default = document.createElement('option');
    Default.innerHTML = "--nuevo--";
    Default.value = -1;
    Default.selected= "selected";
    sel.appendChild(Default);
    var xmlhttp = new XMLHttpRequest();          
    xmlhttp.onreadystatechange=function()
     {               
         if (xmlhttp.readyState==4 && xmlhttp.status==200)                
         {                                        
             var perfiles = JSON.parse(xmlhttp.responseText);
             for(var i = 0; i < perfiles.length; i++) 
             {
                 var opt = document.createElement('option');
                 opt.innerHTML = perfiles[i].nombre;
                 opt.value = perfiles[i].id;
                 sel.appendChild(opt);
             }
         }
     }            
     xmlhttp.open("GET","php/cargarUsuarios.php",true);
     xmlhttp.send();
	
}

function cargarPerfil(){
    //document.getElementById('slcPerfil').options.length  = 0;
    //Selecciona valor por defecto en el select de perfil...
    //remueve las opciones del select.. 
    var sel = document.getElementById('sltPerfil');
    while(sel.length != 0){
        for(var i=0; i < sel.length; i++){
            //sel.removeChild( sel.options[i]); 
            sel.options[i] = null;
        }
    }
    var xmlhttp = new XMLHttpRequest();          
    xmlhttp.onreadystatechange=function()
     {               
         if (xmlhttp.readyState==4 && xmlhttp.status==200)                
         {                                        
             var perfiles = JSON.parse(xmlhttp.responseText);
             for(var i = 0; i < perfiles.length; i++) 
             {
                 var opt = document.createElement('option');
                 opt.innerHTML = perfiles[i].nombre;
                 opt.value = perfiles[i].id;
                 sel.appendChild(opt);
             }
         }
     }            
     xmlhttp.open("GET","php/CargarPerfil.php",true);
     xmlhttp.send();

}

function cargarZonasDisp(){
	var sel = document.getElementById('objLstZnDisp');
    while(sel.length != 0){
        for(var i=0; i < sel.length; i++){
            //sel.removeChild( sel.options[i]); 
            sel.options[i] = null;
        }
    }
    
    var xmlhttp = new XMLHttpRequest();          
    xmlhttp.onreadystatechange=function()
     {               
         if (xmlhttp.readyState==4 && xmlhttp.status==200)                
         {                                        
             var perfiles = JSON.parse(xmlhttp.responseText);
             for(var i = 0; i < perfiles.length; i++) 
             {
                 var opt = document.createElement('option');
                 opt.innerHTML = perfiles[i].znnom;
                 opt.value = perfiles[i].znid;
                 sel.appendChild(opt);
             }
         }
     }            
     xmlhttp.open("GET","php/datosZonas.php",true);
     xmlhttp.send();
}

function iniciaListaUsados(){
	var lstUsr = document.getElementById('objLstZnUsr');
	var length = lstUsr.options.length;
     for (var i = 0; i < length; i++) {
    	 lstUsr.options[0] = null;
     }
}

function validarForm(Pnombre, Pcargo, Plogin, Pmd5, confirmarmd5, Pperfil){
    var objInfo =  document.getElementById("msnError");
    objInfo.style.color = "#BABABA";
    var flgError = false;
    objInfo.innerHTML = "";
    //Obtiene todos los combos y se resaltan de color azul para senalar campos necesarios. 
    var objnombre = document.getElementById("txtNombre");
    var objcargo = document.getElementById("txtCargo");
    var objlogin = document.getElementById("txtLogin");
    var objpsw = document.getElementById("txtPswd");
    var objconfirmacion = document.getElementById("txtConfirmacion");
    if(Pnombre == ""){
         objInfo.innerHTML += "El campo  nombre es necesario<br>";
         objnombre.style.border = '2px solid #3399ff';
         flgError = true;
         return false;
    }
    else{
        objnombre.style.border = 'none';
    }
    if(Pcargo == ""){
         objInfo.innerHTML += "El campo cargo es necesario<br>";
         objcargo.style.border = '2px solid #3399ff';
         flgError = true;
         return false;
    }
    else{
        objcargo.style.border = 'none';
    }

    if(Plogin == ""){
         objInfo.innerHTML += "El campo login es necesario<br>";
         objlogin.style.border = '2px solid #3399ff';
         flgError = true;
         return false;
    }
    else{
        objlogin.style.border = 'none';
    }
    if (Pmd5 == "" ||  confirmarmd5 == "") {
         objInfo.innerHTML += "Los campos Clave y Confirmación son necesarios<br>";
         objpsw.style.border = '2px solid #3399ff';
         objconfirmacion.style.border = '2px solid #3399ff';
         flgError = true;
         return false;
    }
    else{
        objpsw.style.border = 'none';
        objconfirmacion.style.border = 'none';
    }
    if(flgError){
        return false;
    }
    if (Pmd5 != confirmarmd5) {
         objInfo.innerHTML += "La clave ingresada no coincide con la confirmación , verifíquela<br>";
         objpsw.style.border = '2px solid #3399ff';
         objconfirmacion.style.border = '2px solid #3399ff';
         flgError = true;
         return false;
    }
    else{
        objpsw.style.border = 'none';
        objconfirmacion.style.border = 'none';
    }
    if(flgError){
        return false;
    }
    return true;
}
function EliminarUsuario(nombre, cargo, login, md5, perfil){
	var xmlhttp;
    xmlhttp = new XMLHttpRequest();          
    xmlhttp.onreadystatechange=function(){               
        if (xmlhttp.readyState==4 && xmlhttp.status==200)                
        {                       
        	//se obtiene el resultado.                 
            var strresultado = xmlhttp.responseText; 
            if(strresultado == "1")
            	return true;
            else 
            	return false;                  
        }
    };            
    xmlhttp.open("GET","php/usu_elimina.php",true);
    xmlhttp.send();
}

function SeleccionUsuario(nombre, cargo, login, md5, perfil){
	var xmlhttp;
    xmlhttp = new XMLHttpRequest();          
    xmlhttp.onreadystatechange=function(){               
        if (xmlhttp.readyState==4 && xmlhttp.status==200)                
        {                       
        	//se obtiene el resultado.                 
            var strresultado = xmlhttp.responseText; 
            //recorre y llena la el selec de usuario...                
        }
    };            
    xmlhttp.open("GET","php/usu_select.php",true);
    xmlhttp.send();
}

window.onload = function() {
	
	cargarPerfil();
	cargarUsuarios();
	cargarZonasDisp();
	
	

	
    
   /* var usrNm = document.getElementById("txtLogin");
    xmlhttp = new XMLHttpRequest();          
    xmlhttp.onreadystatechange=function(){               
        if (xmlhttp.readyState==4 && xmlhttp.status==200){
        	var res = JSON.parse(xmlhttp.responseText);
        	if(res.length > 0){
        		var xmlhttp1;
        		xmlhttp1 = new XMLHttpRequest();          
        	    xmlhttp1.onreadystatechange=function(){               
        	        if (xmlhttp1.readyState==4 && xmlhttp1.status==200){
        	        	var res1 = JSON.parse(xmlhttp1.responseText);
        	        	if(res1.length > 0){
        	        		var length = lstUsr.options.length;
        	                 for (var i = 0; i < length; i++) {
        	                	 lstUsr.options[0] = null;
        	                 }
        	        		for(var i = 0; i < res1.length; i++){
	        	        		var xmlhttp1;
	        	        		xmlhttp1 = new XMLHttpRequest();          
	        	        	    xmlhttp1.onreadystatechange=function(){               
	        	        	        if (xmlhttp1.readyState==4 && xmlhttp1.status==200){
	        	        	        	var res2 = JSON.parse(xmlhttp1.responseText);
	        	        	        	if(res2.length > 0){
		        	        	        	var opt = document.createElement('option');
		        	                        opt.text = res2[i].znnom;
		        	                        opt.value = res2[i].znid;
		        	                        lstUsr.appendChild(opt);
	        	        	        	}
	        	        	        }
	        	        	    };
	        	        	    xmlhttp.open("GET","php/datosZona.php?znid="+res1[i],true);
	        	        	    xmlhttp.send();
        	        		}
        	        	}
        	        }
        	    };
        	    xmlhttp.open("GET","php/datosZonaUsr.php?usid="+res[0].usid,true);
        	    xmlhttp.send();
        	}
        }
    }
    xmlhttp.open("GET","php/userInfoLog.php?log="+usrNm,true);
    xmlhttp.send();*/
};

function UsrZnAsignar(zona){
	var lstDisp = document.getElementById("objLstZnDisp");
	var lstUsr = document.getElementById("objLstZnUsr");
	
	var opt = lstDisp.options.item(lstDisp.selectedIndex);
    lstUsr.add(opt);
}

function UsrZnEliminar(zona){
	var lstDisp = document.getElementById("objLstZnDisp");
	var lstUsr = document.getElementById("objLstZnUsr");
	
	var opt = lstUsr.options.item(lstUsr.selectedIndex);
	lstDisp.add(opt);
	//lstUsr.remove(lstUsr.selectedIndex);
}

function cargarDatosUSel(sel){
	var lstUsr = document.getElementById('objLstZnUsr');
	var xmlhttp = new XMLHttpRequest();          
    xmlhttp.onreadystatechange=function()
     {               
         if (xmlhttp.readyState==4 && xmlhttp.status==200)                
         {                                        
             var data = JSON.parse(xmlhttp.responseText);
             document.getElementById("txtNombre").value = data[0].nombre;
             document.getElementById("txtCargo").value = data[0].cargo;
             document.getElementById("txtLogin").value = data[0].login;
             document.getElementById("sltPerfil").value = data[0].perfil;
         }
     }            
     xmlhttp.open("GET","php/cargaUsuario.php?id="+sel,true);
     xmlhttp.send();
     
     var usrNm = document.getElementById("txtLogin");
     var xmlhttpT = new XMLHttpRequest();          
     xmlhttpT.onreadystatechange=function(){               
         if (xmlhttpT.readyState==4 && xmlhttpT.status==200){
         	var res = JSON.parse(xmlhttpT.responseText);
         	if(res.length > 0){
         		var xmlhttp1;
         		xmlhttp1 = new XMLHttpRequest();          
         	    xmlhttp1.onreadystatechange=function(){               
         	        if (xmlhttp1.readyState==4 && xmlhttp1.status==200){
         	        	var res1 = JSON.parse(xmlhttp1.responseText);
         	        	if(res1.length > 0){
         	        		var length = lstUsr.options.length;
         	                 for (var i = 0; i < length; i++) {
         	                	 lstUsr.options[0] = null;
         	                 }
         	        		for(var i = 0; i < res1.length; i++){
 	        	        		var xmlhttp2;
 	        	        		xmlhttp2 = new XMLHttpRequest();          
 	        	        	    xmlhttp2.onreadystatechange=function(){               
 	        	        	        if (xmlhttp2.readyState==4 && xmlhttp2.status==200){
 	        	        	        	var res2 = JSON.parse(xmlhttp2.responseText);
 	        	        	        	if(res2.length > 0){
 		        	        	        	var opt = document.createElement('option');
 		        	                        opt.text = res2[i].znnom;
 		        	                        opt.value = res2[i].znid;
 		        	                        lstUsr.appendChild(opt);
 	        	        	        	}
 	        	        	        }
 	        	        	    };
 	        	        	    xmlhttp2.open("GET","php/datosZona.php?znid="+res1[i],true);
 	        	        	    xmlhttp2.send();
         	        		}
         	        	}
         	        }
         	    };
         	    xmlhttp1.open("GET","php/datosZonaUsr.php?usid="+res[0].usid,true);
         	    xmlhttp1.send();
         	}
         }
     }
     xmlhttpT.open("GET","php/userInfoLog.php?log="+usrNm,true);
     xmlhttpT.send();
}
