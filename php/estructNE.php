<?php
require_once ('DBconnect.php');

$Fparent = $_GET ["Fparent"];
$db = new BaseDatos ();

$strSQL = "SELECT NE.CODE,PH.LATITUD,PH.LONGITUD,NT.R,NT.G,NT.B FROM NETOWNERS NE,MVPHNODE PH, NETTYPE NT WHERE NE.PHNODE=PH.CODE AND NT.COMPANY = NE.COMPANY AND PH.CODE IN(SELECT PHNODE FROM MVELNODE WHERE FPARENT = '$Fparent' UNION SELECT PHNODE FROM LVELNODE WHERE FPARENT = '$Fparent')" . " UNION " . "SELECT NE.CODE,PH.LATITUD,PH.LONGITUD,NT.R,NT.G,NT.B FROM NETOWNERS NE,LVPHNODE PH, NETTYPE NT WHERE NE.PHNODE=PH.CODE AND NT.COMPANY = NE.COMPANY AND PH.CODE IN(SELECT PHNODE FROM LVELNODE WHERE FPARENT = '$Fparent')";

/*
 * if($Fparent == "TODOS")
 * {
 * $strSQL = "SELECT NE.CODE,PH.LATITUD,PH.LONGITUD FROM NETOWNERS NE,MVPHNODE PH WHERE NE.PHNODE=PH.CODE".
 * "UNION".
 * "SELECT NE.CODE,PH.LATITUD,PH.LONGITUD FROM NETOWNERS NE,LVPHNODE PH WHERE NE.PHNODE=PH.CODE";
 * }
 */
// error_log(print_r($strSQL, TRUE), 0);
$strucNE = array ();
$res = $db->ejecutar ( $strSQL );
while ( $row = $db->fetch ( $res ) ) {
	$strucNE [] = array (
			'code' => $row [0],
			'lat1' => str_replace ( ",", ".", $row [1] ),
			'lon1' => str_replace ( ",", ".", $row [2] ),
			'r' => str_replace ( ",", ".", $row [3] ),
			'g' => str_replace ( ",", ".", $row [4] ),
			'b' => str_replace ( ",", ".", $row [5] ) ,
			'fparent' => $Fparent 
	);
}

echo json_encode ( $strucNE );
?>