<?php
require_once ('DBconnect.php');

$tabla = $_GET ["TABLA"];
$db = new BaseDatos ();

$datos = array ();
if ($tabla == "NETTYPE") {
	$strSQL = "SELECT COMPANY FROM NETTYPE";
	$res = $db->ejecutar ( $strSQL );
	while ( $row = $db->fetch ( $res ) ) {
		$datos [] = array (
				'code' => $row [0] 
		);
	}
	$db->desconectar ();
	echo json_encode ( $datos );
} else if ($tabla == "CONTRATOS") {
	$strSQL = "SELECT CODCONTRC FROM CONTRATOS";
	$res = $db->ejecutar ( $strSQL );
	while ( $row = $db->fetch ( $res ) ) {
		$datos [] = array (
				'code' => $row [0] 
		);
	}
	$db->desconectar ();
	echo json_encode ( $datos );
}

?>