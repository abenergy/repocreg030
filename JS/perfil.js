function cargarCombos(){
    cargarPerfil();
    cargarCapa();
}

function execute(strSQL)
{
    var xmlhttp;
    xmlhttp = new XMLHttpRequest();          
    xmlhttp.open("GET","php/consulta.php?strSQL="+strSQL+"&token=12345",false);
    xmlhttp.send(); 
    return  JSON.parse(xmlhttp.responseText);     
}

function cargarCapaSeleccionada(valor){
    //TODO: generar todos los elementos que pertenescan a la capa 
    //seleccionada
    //

    var chkContainer = document.getElementById("chkContenedor");
    while (chkContainer.firstChild) {
        chkContainer.removeChild(chkContainer.firstChild);
    }

    var xmlhttp = new XMLHttpRequest(); 
    var url     = "php/web_Perfil_cargarCampo.php";
    var params  = "tabla='"+ valor +"'";
    var selperfil = document.getElementById('slcPerfil');
    
    var objInfo =  document.getElementById("msnError");
    objInfo.style.color = "#BABABA";


    xmlhttp.onreadystatechange=function()
     {               
         if (xmlhttp.readyState==4 && xmlhttp.status==200)                
         {                                        
             var capas = JSON.parse(xmlhttp.responseText);
             var sel = document.getElementById('slcCapas');
             var container = document.getElementById('chkContenedor');
             var objOpciones;
             if(selperfil.value != "-1"){
                objOpciones = execute("SELECT ID_CAMPO FROM web_per_perfil WHERE ID_PERFIL=" + selperfil.value);    
                objInfo.innerHTML = "";
             }
             else{
                objInfo.innerHTML = "Es necesario seleccionar un perfil para cargar las opciones";
             }
             for(var i = 0; i < capas.length; i++) 
             {
                
                var opt = document.createElement('input');
                opt.setAttribute('id',capas[i].id);
                opt.setAttribute('type','checkbox');
                opt.value = capas[i].nombre;
                opt.innerHTML = capas[i].nombre;
                container.appendChild(opt);
                var lab = document.createTextNode(capas[i].nombre);
                container.appendChild(lab);
                var br = document.createElement('br');
                container.appendChild(br);

                if(objOpciones != null){
                    for(var y = 0; y < objOpciones.length ; y++)
                    {
                        if(objOpciones[y].ID_CAMPO == opt.id){
                            opt.checked = true;
                        }
                    }
                }

             }
         }
     }      
     xmlhttp.open("GET",url+"?"+params,true);
     xmlhttp.send();
}


function cargarCapa(){
    var xmlhttp = new XMLHttpRequest();          
    xmlhttp.onreadystatechange=function()
     {               
         if (xmlhttp.readyState==4 && xmlhttp.status==200)                
         {                                        
             var capas = JSON.parse(xmlhttp.responseText);
             var sel = document.getElementById('slcCapas');
             for(var i = 0; i < capas.length; i++) 
             {
                 var optCapa = document.createElement('option');
                 optCapa.innerHTML = capas[i].nombre;
                 optCapa.value = capas[i].nombre;
                 sel.appendChild(optCapa);
             }
         }
     }            
     xmlhttp.open("GET","php/CargarCapas.php",true);
     xmlhttp.send();
}

function cargarPerfil(){
    //document.getElementById('slcPerfil').options.length  = 0;
    //Selecciona valor por defecto en el select de perfil...
    //remueve las opciones del select.. 
    var sel = document.getElementById('slcPerfil');
    while(sel.length != 0){
        for(var i=0; i < sel.length; i++){
            //sel.removeChild( sel.options[i]); 
            sel.options[i] = null;
        }
    }
    var Default = document.createElement('option');
                 Default.innerHTML = "--nuevo--";
                 Default.value = -1;
                 Default.selected= "selected";
                 sel.appendChild(Default);
    var xmlhttp = new XMLHttpRequest();          
    xmlhttp.onreadystatechange=function()
     {               
         if (xmlhttp.readyState==4 && xmlhttp.status==200)                
         {                                        
             var perfiles = JSON.parse(xmlhttp.responseText);
             for(var i = 0; i < perfiles.length; i++) 
             {
                 var opt = document.createElement('option');
                 opt.innerHTML = perfiles[i].nombre;
                 opt.value = perfiles[i].id;
                 sel.appendChild(opt);
             }
         }
     }            
     xmlhttp.open("GET","php/CargarPerfil.php",true);
     xmlhttp.send();

}

function OcultaAdmin()
{
    var admin = document.getElementById("frmAdmin");
    admin.style.visibility = 'hidden';
}

$(document).ready(function() {
  cargarCombos();
});
function limpiarMensaje(){
    var objInfo =  document.getElementById("msnError");
    objInfo.style.color = "#BABABA";
    objInfo.innerHTML = "";
}
function guardarPerfil(Pid,Pnombre){
    //TODO:  REALIZAR VALIDACIONES PARA EVITAR DATOS INCONSISTENTES... !!!1 IMPORTANT.
    var objInfo =  document.getElementById("msnError");
    var objnombre = document.getElementById("txtNombre");
    objInfo.style.color = "#BABABA";
    objInfo.innerHTML = "";
    if(Pnombre == "" || Pnombre == null){
        objInfo.innerHTML = "El campo nombre no puede estar vacio";
        objnombre.style.border = '2px solid #3399ff';
        return;
    }
    else{
        objnombre.style.border = 'none';
    }
    var xmlhttp;
    var url    = "php/crearPerfil.php";
    var params = "id="+Pid+"&nombre="+Pnombre;
    xmlhttp = new XMLHttpRequest();   
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200){                       
            var strresultado = xmlhttp.responseText; 
            if(strresultado == "1"){
                //cargar valor guardado en el select.
                objInfo.innerHTML = 'El perfil a sido guardado';
                var slcPerfilDato = document.getElementById("slcPerfil");
                for (var i = 0; i < slcPerfilDato.length; i++) {
                    //Aca haces referencia al "option" actual
                    var opt = slcPerfilDato[i];
                    if(opt.value == Pnombre)
                    {
                        opt.selected = 'selected';
                        return true;
                    }
                }
                cargarPerfil();
                return true;
            }
            else 
                return false;                  
        }
    };
    xmlhttp.open("GET", url+"?"+params,true);
    xmlhttp.send();
}

function guardarContenido(Pid_perfil){
    //TODO:  REALIZAR VALIDACIONES PARA EVITAR DATOS INCONSISTENTES... !!!1 IMPORTANT.
    //1. recorrer check y saber cuales estan seleccionados.
    var objInfo =  document.getElementById("msnError"); 
    objInfo.style.color = "#BABABA";
    if(Pid_perfil == "-1"){
        objInfo.innerHTML = 'Es necesario seleccionar un perfil en el Combo de Selección';
        return;
    }
    else{
        objInfo.innerHTML = "";
    }
    
    var chkArreglo = [];
    var Pid_campo = '';
    $('#chkContenedor input:checked').each(function() {
        chkArreglo.push($(this).attr('id'));
    });
    for (var i = 0; i < chkArreglo.length; i++) {
        Pid_campo += '|' + chkArreglo[i];
    };
    var xmlhttp;
    var url    = "php/web_per_perfil.php";
    var params = "id_perfil="+Pid_perfil+"&id_campo="+Pid_campo;
    xmlhttp = new XMLHttpRequest();  
    
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200){                       
            var strresultado = xmlhttp.responseText; 
            if(strresultado == "1"){
                //cargar valor guardado en el select.
                objInfo.innerHTML = 'El perfil a sido guardado';
                var slcPerfilDato = document.getElementById("slcPerfil");
                for (var i = 0; i < slcPerfilDato.length; i++) {
                    //Aca haces referencia al "option" actual
                    var opt = slcPerfilDato[i];
                    if(opt.value == Pid_perfil)
                    {
                        opt.selected = 'selected';
                        return true;
                    }
                }
                return true;
            }
            else 
                return false;                  
        }
    };
    xmlhttp.open("GET", url+"?"+params,true);
    xmlhttp.send();
}

