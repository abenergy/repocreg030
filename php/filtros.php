<?php
require_once ('DBconnect.php');

if(!empty($_GET["token"])){
	$token=$_GET["token"];
	$db = new BaseDatos ();
	if($db->validate_token($token)){

		$strSQL = "SELECT FIL_ID,FIL_NOMBRE,FIL_CONTENIDO,FIL_TIPO FROM FIL_FILTRO ORDER BY FIL_ID";
		// error_log(print_r($strSQL, TRUE), 0);
		$Feeders = array ();
		$res = $db->ejecutar ( $strSQL );
		while ( $row = $db->fetch ( $res ) ) {
			$Feeders [] = array (
					'fil_id' => $row [0],
					'fil_nombre' => $row[1] ,
					'fil_contenido' => $row[2],
					'fil_tipo' => $row[3]
			);
		}
		echo json_encode ( $Feeders );
	}
}
?>
