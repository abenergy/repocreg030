var filtros;
var token;

function dir(object) {
	stuff = [];
	for (s in object) {
		stuff.push(s);
	}
	stuff.sort();
	return stuff;
}

function hideShowGraf(div){
	$('#'+div).toggle('slide');
}

var contXmlHttpFil = 0;
var datos_cc = [];
var html_form = '';
function armaHtmlFormFil(){
	try{
		var xmlhttp2;
		var arr = null;
		var fil_nombre = datos_cc[contXmlHttpFil].fil_nombre;
		var fil_id = datos_cc[contXmlHttpFil].fil_id;
		var xmlhttp2 = new XMLHttpRequest();          
		xmlhttp2.onreadystatechange=function()
		 {               
			 if (xmlhttp2.readyState==4 && xmlhttp2.status==200)                
			 {
				 var res = xmlhttp2.responseText;
				 res = res.substring(0,res.length -1);
				 res += "]";
				 
				arr = JSON.parse(res);
				
				html_form += fil_nombre+': <select id="'+fil_id+'">';
				for(j = 0; j < arr.length; j++){
					keys_arr = Object.keys(arr[j]);
					if(keys_arr.length > 1){
						value_option = arr[j][keys_arr[1]];
						descript_option = arr[j][keys_arr[0]];
					}else{
						value_option = arr[j][keys_arr[0]];
						descript_option = arr[j][keys_arr[0]];
					}
					html_form += '<option value="'+value_option+'">'+descript_option+'</option>';
				}
				html_form += '</select><br>';
				contXmlHttpFil += 1;
				if(contXmlHttpFil < datos_cc.length){
					armaHtmlFormFil();
				}
			 }
		 }
		xmlhttp2.open("GET","PHP/consulta2.php?strSQL="+datos_cc[contXmlHttpFil].fil_contenido+"&token="+token,true);
		xmlhttp2.send();
	}catch(err){
		$.LoadingOverlay("hide");
	}
}

function FormularioDinamico(valor){
	$.LoadingOverlay("show");
	try{
		var xmlhttp;
		xmlhttp = new XMLHttpRequest();          
		xmlhttp.onreadystatechange=function()
		 {               
			 if (xmlhttp.readyState==4 && xmlhttp.status==200)                
			 {                                        
				 //CargarFeeders(xmlhttp.responseText);
				 filtros = JSON.parse(xmlhttp.responseText);
				 var datos = [];
				 html_form = '';
				 var xmlhttp_arr = [];
				 for(var i = 0; i < filtros.length; i++) 
				 {
					if(valor.indexOf(filtros[i].fil_id) != -1){
						if(filtros[i].fil_tipo == 'TX'){
							datos.push(filtros[i]);
							html_form += filtros[i].fil_nombre+': <input id="'+filtros[i].fil_id+'" type="text"><br>';
						}else{
							if(filtros[i].fil_tipo == 'CC'){
								datos.push(filtros[i]);
								datos_cc.push(filtros[i]);
							}else{
								//alert(filtros[i].fil_tipo);
								if(filtros[i].fil_tipo == 'FC'){
									datos.push(filtros[i]);
									html_form += filtros[i].fil_nombre+': <div class="input-group date form_datetime" data-date="1979-09-16T05:25:07Z" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">'+
										'<input class="form-control" size="16" type="text" value="" id="'+filtros[i].fil_id+'">'+
										'<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>'+
										'<span class="input-group-addon"><i class="fa fa-times" aria-hidden="true"></i></span>'+
									'</div><br>';
								}
							}
						}
					}
				 }
				 armaHtmlFormFil();
				 window.setTimeout(function(){
					 if (html_form != ''){
						 swal({
						  title: '',
						  html: html_form,
						  //~ html: true,
						  //~ text: html_form,
						  showConfirmButton: true,
						  //customClass: 'custom-swal-class',
						  onOpen: function() {
							  $('.form_datetime').datetimepicker({
								language:  'es',
								format: 'yyyy-mm-dd hh:ii',
								weekStart: 1,
								todayBtn:  1,
								autoclose: 1,
								todayHighlight: 1,
								startView: 2,
								forceParse: 0,
								showMeridian: 1
								}).on('hide', function(e) {
									e.stopPropagation();
								});
						  },
						}).then(function(result) {
							for (var k = 0; k < datos.length; k++){
								var input = document.getElementById(datos[k].fil_id);
								datos[k].valor = input.value;
							}
							for (var k = 0; k < datos.length; k++){
								//alert(datos[i].valor);
								valor = valor.replace(datos[k].fil_id,datos[k].valor)
							}
							CargarReporte(valor);
						  });
					 }else{
						 CargarReporte(valor);
					 }
					 $.LoadingOverlay("hide");
				 },2000);
			 }
		 }            
		 xmlhttp.open("GET","PHP/filtros.php?token="+token,true);
		 xmlhttp.send();
	 }catch(err){
		 $.LoadingOverlay("hide");
	 }
}

function CargarReporte(valor)
	{
		$.LoadingOverlay("show");
		try{
			var xmlhttp;
			xmlhttp = new XMLHttpRequest();          
			xmlhttp.onreadystatechange=function()
			 {          
				
				 if (xmlhttp.readyState==4 && xmlhttp.status==200)                
				 {
					 var res = xmlhttp.responseText;
					 res = res.substring(0,res.length -1);
					 res += "]";
					 
					 var titulos = valor.substring(7,valor.indexOf("FROM") - 1)
					 try{
						var arr = JSON.parse(res);
						var i;
						var out = '<table class="table table-bordered dataTable display" id="idDataGrid">';
						var Linea;
						var x;
						var titles = titulos.split(",");
						out += "<thead>";
						out += "<tr>";
						var labels_graf = [];
						var datasets_graf = [];
						var datasets_graf_pie = [];
						for(i = 0; i < titles.length; i++)
						{
							Linea = titles[i];
							out += "<th>";
							out+= Linea;
							out+="</th>";
							if(i != 0){
								if(i == 1){
									var r = 0;
									var g = 0;
									var b = 256;
									var a = 1;
								}else{
									if(i == 2){
										var r = 256;
										var g = 0;
										var b = 0;
										var a = 1;
									}else{
										if(i == 3){
											var r = 0;
											var g = 256;
											var b = 0;
											var a = 1;
										}else{
											var r = Math.floor(Math.random()*256);
											var g = Math.floor(Math.random()*256);
											var b = Math.floor(Math.random()*256);
											var a = Math.floor(Math.random()*256);
										}
									}
								}
								datasets_graf.push({label: titles[i],
									lineTension: 0.3,
									backgroundColor: "rgba("+r+","+g+","+b+","+a+")",
									borderColor: "rgba("+r+","+g+","+b+","+a+")",
									pointRadius: 5,
									pointBackgroundColor: "rgba("+r+","+g+","+b+","+a+")",
									pointBorderColor: "rgba(255,255,255,0.8)",
									pointHoverRadius: 5,
									pointHoverBackgroundColor: "rgba("+r+","+g+","+b+","+a+"",
									pointHitRadius: 20,
									pointBorderWidth: 2,
									data:[],
									fill: false});
								datasets_graf_pie.push({label: titles[i],
									lineTension: 0.3,
									backgroundColor: [],
									borderColor: [],
									pointRadius: 5,
									pointBackgroundColor: "rgba("+r+","+g+","+b+","+a+")",
									pointBorderColor: "rgba(255,255,255,0.8)",
									pointHoverRadius: 5,
									pointHoverBackgroundColor: "rgba("+r+","+g+","+b+","+a+"",
									pointHitRadius: 20,
									pointBorderWidth: 2,
									data:[],
									fill: false});
							}
						}
						out += "</tr>";
						out += "</thead>";
						//document.getElementById("id01").innerHTML = out;
						for(i = 0; i < arr.length; i++)
						{
							Linea = arr[i];
							out += "<tr>";
							var cont = 0;
							for (x in Linea)
							{
								out += "<td>" +  Linea[x] + "</td>";
								//~ alert(cont);
								if(cont != 0 && cont <= datasets_graf.length){
									datasets_graf[cont-1].data.push(Linea[x]);
									datasets_graf_pie[cont-1].data.push(Linea[x]);
									if(i == 1){
										var r = 0;
										var g = 0;
										var b = 256;
										var a = 1;
									}else{
										if(i == 2){
											var r = 256;
											var g = 0;
											var b = 0;
											var a = 1;
										}else{
											if(i == 3){
												var r = 0;
												var g = 256;
												var b = 0;
												var a = 1;
											}else{
												var r = Math.floor(Math.random()*256);
												var g = Math.floor(Math.random()*256);
												var b = Math.floor(Math.random()*256);
												var a = Math.floor(Math.random()*256);
											}
										}
									}
									datasets_graf_pie[cont-1].backgroundColor.push("rgba("+r+","+g+","+b+","+a+")");
									datasets_graf_pie[cont-1].borderColor.push("rgba(256,256,256,256)");
								}else{
									labels_graf.push(Linea[x]);
								}
								cont += 1;
							}
							out+="</td>";
						}
						out += "</table>";
						document.getElementById("id01").innerHTML = out;
						$('#idDataGrid').DataTable({
							dom: 'Bfrtip',
							buttons: [
								'copyHtml5',
								'excelHtml5',
								'csvHtml5',
								'pdfHtml5'
							]
						});
						//alert(labels_graf);
						var datasets_line = datasets_graf;
						var datasets_bar = datasets_graf;
						var datasets_pie = datasets_graf_pie;
						var graf_area = 
						'<button type="button" class="swal2-confirm swal2-styled" style="background-color: rgb(48, 133, 214); border-left-color: rgb(48, 133, 214); border-right-color: rgb(48, 133, 214);" onclick="hideShowGraf('+"'"+'divArea'+"'"+')"><i class="fa fa-area-chart"></i></button>'+
						'<div class="card mb-3" style="display:none;resize:both;overflow:auto;width:50%;" id="divArea">'+
							'<div class="card-header" >'+
								'<i class="fa fa-area-chart"></i> Area Chart</div>'+
							'<div class="card-body">'+
								'<canvas id="myAreaChart" width="50%" height="30"></canvas>'+
							'</div>'+
						'</div>';
						graf_area += 
						'<button type="button" class="swal2-confirm swal2-styled" style="background-color: rgb(48, 133, 214); border-left-color: rgb(48, 133, 214); border-right-color: rgb(48, 133, 214);" onclick="hideShowGraf('+"'"+'divBar'+"'"+')"><i class="fa fa-bar-chart"></i></button>'+
						'<div class="card mb-3" style="display:none;resize:both;overflow:auto;width:50%;" id="divBar">'+
							'<div class="card-header">'+
							  '<i class="fa fa-bar-chart"></i> Bar Chart</div>'+
							'<div class="card-body">'+
							  '<canvas id="myBarChart" width="50%"></canvas>'+
							'</div>'+
						  '</div>';
						graf_area += 
						'<button type="button" class="swal2-confirm swal2-styled" style="background-color: rgb(48, 133, 214); border-left-color: rgb(48, 133, 214); border-right-color: rgb(48, 133, 214);" onclick="hideShowGraf('+"'"+'divPie'+"'"+')"><i class="fa fa-pie-chart"></i></button>'+
						'<div class="card mb-3" style="display:none;resize:both;overflow:auto;width:50%;" id="divPie">'+
							'<div class="card-header">'+
							  '<i class="fa fa-pie-chart"></i> Pie Chart</div>'+
							'<div class="card-body">'+
							  '<canvas id="myPieChart" width="50%"></canvas>'+
							'</div>'+
						  '</div>';
						document.getElementById("Graficos").innerHTML = graf_area;
						var ctx = document.getElementById("myAreaChart");
						var myLineChart = new Chart(ctx, {
						  type: 'line',
						  data: {
							labels: labels_graf,
							datasets: datasets_line,
						  },
						});
						
						var ctx = document.getElementById("myBarChart");
						var myBarChart = new Chart(ctx, {
						  type: 'bar',
						  data: {
							labels: labels_graf,
							datasets: datasets_bar,
						  },
						});
						
						var ctx = document.getElementById("myPieChart");
						var myPieChart = new Chart(ctx, {
						  type: 'pie',
						  data: {
							labels: labels_graf,
							datasets: datasets_pie,
						  },
						});
					}catch(err){
						swal("No se encontraron datos");
					}
					$.LoadingOverlay("hide");
				 }
			 }
			 //xmlhttp.open("GET","php/consulta.php?strSQL="+valor+"&token=12345",false);
			 valor = valor.toUpperCase();
			 xmlhttp.open("GET","PHP/consulta2.php?token="+token+"&strSQL="+valor,true);
			 xmlhttp.send();
		 }catch(err){
			 $.LoadingOverlay("hide");
		 }
	}
	
	function init()
	{
		var xmlhttp;
        xmlhttp = new XMLHttpRequest();          
        xmlhttp.onreadystatechange=function()
         {               
             if (xmlhttp.readyState==4 && xmlhttp.status==200)                
             {                                        
                 //CargarFeeders(xmlhttp.responseText);
            	 var feeders = JSON.parse(xmlhttp.responseText);
            	 var sel = document.getElementById('LstReportes');
                 for(var i = 0; i < feeders.length; i++) 
                 {
                     var opt = document.createElement('option');
                     opt.innerHTML = feeders[i].nom;
                     opt.value = feeders[i].sql;
                     sel.appendChild(opt);
                 }
             }
         }            
         token = sessionStorage.getItem('lstoken');
         xmlhttp.open("GET","PHP/reportes.php?token="+token,true);
         xmlhttp.send();
	}
	
	
	function myFunction(response)
	{
	    var arr = JSON.parse(response);
	    var i;
	    var out = "<table>";
	    var Linea;
	    var x;
	    for(i = 0; i < arr.length; i++)
	    {
	        Linea = arr[i];
	        out += "<tr><td>"; 
	        for (x in Linea)
	        {
	            out +=Linea[x] +"</td><td>";                 
	        }
	        out+="</td></tr>";
	    }
	    out += "</table>";
	    document.getElementById("id01").innerHTML = out;
	}
	
	window.onload = init;
