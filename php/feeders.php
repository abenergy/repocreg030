<?php
require_once ('DBconnect.php');

$db = new BaseDatos ();

$strSQL = "select code, latitud, longitud,r,g,b from feeders";
// error_log(print_r($strSQL, TRUE), 0);
$Feeders = array ();
$res = $db->ejecutar ( $strSQL );
while ( $row = $db->fetch ( $res ) ) {
	$Feeders [] = array (
			'code' => $row [0],
			'lat' => str_replace ( ",", ".", $row [1] ),
			'lon' => str_replace ( ",", ".", $row [2] ),
			'r' => str_replace ( ",", ".", $row [3] ),
			'g' => str_replace ( ",", ".", $row [4] ),
			'b' => str_replace ( ",", ".", $row [5] ) 
	);
}
echo json_encode ( $Feeders );
?>
