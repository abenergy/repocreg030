<?php
class Util
{
	public function validaUser($token){
		$bd = BaseDatos::getInstance();
		$res = $bd->ejecutar("SELECT USU_LOGIN, MD5 FROM GEV_USU_USUARIO WHERE token='$token'");
		$bd->fetch($res);
		$nlin = $bd->nrows($res);
		//$bd->desconectar();
		if ($nlin > 0){	return true;}
		return false;
	}
	
	public function validaCuadrilla($idCuadrilla){
		//error_log(print_r("INICIO Validar Cuadrilla", TRUE), 0);
		$bd = BaseDatos::getInstance();
		$res = $bd->ejecutar("SELECT CUA_ID, USU_CLAVE FROM GEV_USU_USUARIO WHERE TOKEN='$idCuadrilla'");
		//CONSULTA ORIGINAL DE BASE DE DATOS
		//$res = $bd->ejecutar("SELECT CUA_ID, USU_CLAVE FROM GEV_USU_USUARIO WHERE TOKEN='$idCuadrilla'");
		//error_log(print_r("SELECT CUA_ID, USU_CLAVE FROM GEV_USU_USUARIO WHERE USU_LOGIN='$idCuadrilla'", TRUE), 0);
		$row = $bd->fetch($res);
		if ($bd->nrows($res) > 0)
		{
			$rowF = $row[0];
			//error_log(print_r("Validar Cuadrilla retorno ".$rowF, TRUE), 0);
			//$bd->desconectar();
			return $rowF;
		}
		else{
			//error_log(print_r("Validar Cuadrilla retorno -1", TRUE), 0);
			//$bd->desconectar();
			return -1;
		}
	}
	
	public function getUser($token){
		$bd = BaseDatos::getInstance();
		$res = $bd->ejecutar("SELECT USU_ID FROM GEV_USU_USUARIO WHERE token='$token'");
		$bd->fetch($res);
		if ($bd->nrows($res)>0){
			$row = $bd->fetch($res);
			//$bd->desconectar();
			return $row[0];
		}
		//$bd->desconectar();
		return "NaN";
	}
	
	public function obtieneCircuitos(){
		$db = BaseDatos::getInstance();
		$listaFeed = array();
		$res = $db->ejecutar("SELECT CODE FROM FEEDERS");
		while ($row = $db->fetch($res)){
			//$listaFeed[] = array('str'=>$row[0],);
			array_push($listaFeed, $row[0]);
		}
		//$db->desconectar();
		return $listaFeed;
	}
	//TODO:adicionar un metodo que inserte los datos de usuario y fecha cada vez que se llama un metodo get del servicio
	public function auditar($token, $codeElemento, $opcion, $tableName){
		$db = BaseDatos::getInstance();
		error_log(print_r("Creando registro Auditoria en CDI_LOG_PDA", TRUE), 0);
		$res = $db->ejecutar("SELECT USU_ID, MD5 FROM GEV_USU_USUARIO WHERE token='$token'");
		$row = $db->fetch($res);
		$strDate = date("Y-m-d H:i:s");
		if ($db->dbtype == 0){
			$res1 = $db->ejecutar("SELECT COUNT(*) FROM CDI_LOG_PDA WHERE COD_ELEMENTO='$codeElemento' AND CDI_OPCION=$opcion AND NMB_TABLA='$tableName'");
			$row1 = $db->fetch($res1);
			if($row1[0] == 0)
				$strSQL = "INSERT INTO CDI_LOG_PDA(COD_ELEMENTO,CDI_OPCION,NMB_TABLA,CDI_USER,CDI_DATE) ".
					"VALUES('$codeElemento',$opcion,'$tableName','$row[0]','$strDate')";
			else 
				$strSQL = "UPDATE CDI_LOG_PDA SET CDI_USER='$row[0]',CDI_DATE='$strDate' WHERE COD_ELEMENTO='$codeElemento' AND CDI_OPCION=$opcion AND NMB_TABLA='$tableName'"; 
		}
		else{
			$res1 = $db->ejecutar("SELECT COUNT(*) FROM CDI_LOG_PDA WHERE COD_ELEMENTO='$codeElemento' AND CDI_OPCION=$opcion AND NMB_TABLA='$tableName'");
			$row1 = $db->fetch($res1);
			if($row1[0] == 0)
				$strSQL = "INSERT INTO CDI_LOG_PDA(COD_ELEMENTO,CDI_OPCION,NMB_TABLA,CDI_USER,CDI_DATE) ".
					"VALUES('$codeElemento',$opcion,'$tableName','$row[0]',to_date('$strDate','YYYY-MM-DD HH24:MI:SS'))";
			else
				$strSQL = "UPDATE CDI_LOG_PDA SET CDI_USER='$row[0]',CDI_DATE=to_date('$strDate','YYYY-MM-DD HH24:MI:SS') WHERE COD_ELEMENTO='$codeElemento' AND CDI_OPCION=$opcion AND NMB_TABLA='$tableName'";
		}
		error_log(print_r($strSQL, TRUE), 0);
		$db->ejecutar($strSQL);
		//$db->desconectar();
	}
	
	public function insertTablaHistoricoNetOwner($code,$token)
	{
		error_log(print_r("Tabla Historico Netowner", TRUE), 0);
		$db = BaseDatos::getInstance();
		$user = "";
		$str = "";
		
		$res = $db->ejecutar("SELECT USU_ID, MD5 FROM GEV_USU_USUARIO WHERE token='$token'");
		if($row = $db->fetch($res))
		{
			$user = $row[0];
		}
		$strDate = date("Y-m-d H:i:s");
		
		$strSQL = "SELECT CODE, PHNODE, COMPANY, HEIGHT, USER_,FECHAL,DESCRIPTIO,CUADRILLA,NIVEL,TYPE,CONTRATO FROM NETOWNERS WHERE CODE='$code'";
		error_log(print_r($strSQL, TRUE), 0);
		$res = $db->ejecutar($strSQL);
		if($row = $db->fetch($res))
		{
			if ($db->dbtype == 0)
			{
				$str = "INSERT INTO NETOWNERSH(CODE, PHNODE, COMPANY, HEIGHT, USER_,FECHAL,DESCRIPTIO,CUADRILLA,NIVEL,TYPE,CONTRATO,USUARIO,FECHA) ".
					"VALUES('$row[0]','$row[1]','$row[2]',$row[3],'$row[4]','$row[5]','$row[6]','$row[7]',$row[8],'$row[9]','$row[10]','$user','$strDate')";
			}
			else
			{
				$str = "INSERT INTO NETOWNERSH(CODE, PHNODE, COMPANY, HEIGHT, USER_,FECHAL,DESCRIPTIO,CUADRILLA,NIVEL,TYPE,CONTRATO,USUARIO,FECHA) ".
						"VALUES('$row[0]','$row[1]','$row[2]',$row[3],'$row[4]',to_date('$row[5]','YYYY-MM-DD HH24:MI:SS'),'$row[6]','$row[7]',$row[8],'$row[9]','$row[10]','$user',to_date('$strDate','YYYY-MM-DD HH24:MI:SS'))";
			}
			error_log(print_r($str, TRUE), 0);
			
			$res = $db->ejecutar($str);
		}
		//$db->desconectar();
	}
	
	public function insertTablaHistoricaStreetlg($code,$token)
	{
		error_log(print_r("Tabla Historico Streetlg", TRUE), 0);
		$db = BaseDatos::getInstance();
		$user = "";
		$str = "";
	
		$res = $db->ejecutar("SELECT USU_ID, MD5 FROM GEV_USU_USUARIO WHERE token='$token'");
		if($row = $db->fetch($res))
		{
			$user = $row[0];
		}
		$strDate = date("Y-m-d H:i:s");
	
		$strSQL = "SELECT CODE,DESCRIPTIO,ELNODE,TYPE,KW,KV,ASSEMBLY,SYMBOL,USER_,TPARENT,FPARENT,XPOS,YPOS,".
				"DATUM,XSIZE,YSIZE,PHASES,ADDRESS,PROJECT,METERCODE,CONTROL,PRSTATE,PERDIDAS,POWERFAC,".
				"LATITUD,LONGITUD,POSICION,MEDIDA,F_UTILIZA FROM STREETLG WHERE CODE='$code'";
		error_log(print_r($strSQL, TRUE), 0);
		$res = $db->ejecutar($strSQL);
		if($row = $db->fetch($res))
		{
			if ($db->dbtype == 0)
			{
				$str = "INSERT INTO STREETLG_H (CODE,DESCRIPTIO,ELNODE,TYPE,KW,KV,ASSEMBLY,SYMBOL,USER_,TPARENT,FPARENT,XPOS,YPOS,".
						"DATUM,XSIZE,YSIZE,PHASES,ADDRESS,PROJECT,METERCODE,CONTROL,PRSTATE,PERDIDAS,POWERFAC,".
						"LATITUD,LONGITUD,POSICION,MEDIDA,F_UTILIZA,USUARIO,FECHA) ".
						"VALUES('$row[0]','$row[1]','$row[2]','$row[3]',". str_replace(',','.',$row[4]) .",". str_replace(',','.',$row[5]) .",'$row[6]',".
						"$row[7],'$row[8]','$row[9]','$row[10]',". str_replace(',','.',$row[11]) .",". str_replace(',','.',$row[12]) .",$row[13],$row[14],".
						"$row[15],$row[16],'$row[17]','$row[18]','$row[19]',$row[20],$row[21],". str_replace(',','.',$row[22]) .",". str_replace(',','.',$row[23]) .",".
						"". str_replace(',','.',$row[24]) .",". str_replace(',','.',$row[25]) .",$row[26],'$row[27]',". str_replace(',','.',$row[28]) .",".
						"'$user','$strDate')";
			}
			else if ($db->dbtype == 1)
			{
				$str = "INSERT INTO STREETLG_H (CODE,DESCRIPTIO,ELNODE,TYPE,KW,KV,ASSEMBLY,SYMBOL,USER_,TPARENT,FPARENT,XPOS,YPOS,".
						"DATUM,XSIZE,YSIZE,PHASES,ADDRESS,PROJECT,METERCODE,CONTROL,PRSTATE,PERDIDAS,POWERFAC,".
						"LATITUD,LONGITUD,POSICION,MEDIDA,F_UTILIZA,USUARIO,FECHA) ".
						"VALUES('$row[0]','$row[1]','$row[2]','$row[3]',". str_replace(',','.',$row[4]) .",". str_replace(',','.',$row[5]) .",'$row[6]',".
						"$row[7],'$row[8]','$row[9]','$row[10]',". str_replace(',','.',$row[11]) .",". str_replace(',','.',$row[12]) .",$row[13],$row[14],".
						"$row[15],$row[16],'$row[17]','$row[18]','$row[19]',$row[20],$row[21],". str_replace(',','.',$row[22]) .",". str_replace(',','.',$row[23]) .",".
						"". str_replace(',','.',$row[24]) .",". str_replace(',','.',$row[25]) .",$row[26],'$row[27]',". str_replace(',','.',$row[28]) .",".
						"'$user',to_date('$strDate','YYYY-MM-DD HH24:MI:SS'))";
			}
			error_log(print_r($str, TRUE), 0);
				
			$res = $db->ejecutar($str);
		}
		//$db->desconectar();
	}
	
	//lvlinsec(0),lvphnode(1),lvelnode(2),custmetr(3),cmbanks(4)
	public function getTotalElementos($tipo, $fparent){
		$db = BaseDatos::getInstance();
		$strSQL = "";
		error_log(print_r("Conteo de elementos", TRUE), 0);
		switch ($tipo){
			case 0:
				error_log(print_r("elemento encontrado LVLINSEC", TRUE), 0);
				$strSQL = "SELECT COUNT(*) FROM LVLINSEC WHERE FPARENT='$fparent'";
				$res = $db->ejecutar($strSQL);
				$row = $db->fetch($res);
				error_log(print_r("TOTAL LVLINSEC $row[0]", TRUE), 0);
				//$db->desconectar();
				return array('tipo'=>$tipo,'valor'=>$row[0],);
				break;
			case 1:
				error_log(print_r("elemento encontrado LVPHNODE", TRUE), 0);
				$strSQL = "SELECT COUNT(*) FROM LVPHNODE WHERE FPARENT='$fparent'";
				$res = $db->ejecutar($strSQL);
				$row = $db->fetch($res);
				error_log(print_r("TOTAL LVPHNODE $row[0]", TRUE), 0);
				//$db->desconectar();
				return array('tipo'=>$tipo,'valor'=>$row[0],);
				break;
			case 2:
				error_log(print_r("elemento encontrado LVELNODE", TRUE), 0);
				$strSQL = "SELECT COUNT(*) FROM LVELNODE WHERE FPARENT='$fparent'";
				$res = $db->ejecutar($strSQL);
				$row = $db->fetch($res);
				error_log(print_r("TOTAL LVELNODE $row[0]", TRUE), 0);
				//$db->desconectar();
				return array('tipo'=>$tipo,'valor'=>$row[0],);
				break;
			case 3:
				error_log(print_r("elemento encontrado CUSTMETR", TRUE), 0);
				$strSQL = "SELECT COUNT(*) FROM CUSTMETR WHERE FPARENT='$fparent'";
				$res = $db->ejecutar($strSQL);
				$row = $db->fetch($res);
				error_log(print_r("TOTAL CUSTMETR $row[0]", TRUE), 0);
				//$db->desconectar();
				return array('tipo'=>$tipo,'valor'=>$row[0],);
				break;
			case 4:
				error_log(print_r("elemento encontrado CMBANKS", TRUE), 0);
				$strSQL = "SELECT COUNT(*) FROM CMBANKS WHERE FPARENT='$fparent'";
				$res = $db->ejecutar($strSQL);
				$row = $db->fetch($res);
				error_log(print_r("TOTAL CMBANKS $row[0]", TRUE), 0);
				//$db->desconectar();
				return array('tipo'=>$tipo,'valor'=>$row[0],);
				break;
		}
		return 0;
	}
	
	public function guardaImagen($infoFile, $nomFile)
	{
		$location = ".\Fotos\\".$nomFile;                               // Mention where to upload the file
		$current = file_get_contents($location);                     // Get the file content. This will create an empty file if the file does not exist
		//$current = base64_decode($infoFile);                          // Now decode the content which was sent by the client
		//file_put_contents($location, $current);
		file_put_contents($location, $infoFile);
	} 
}

?>