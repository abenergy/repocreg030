<?php
class BaseDatos {
	private $conexion;
	public $dbtype;
	private $host;
	private $port;
	private $dbname;
	private $user;
	private $password;
	private $oraservice;
	public $cantidad = 500;
	public $ext = 0;
	public function __construct() {
		$xmlD = new DOMDocument ( "1", "ISO-8859-1" );
		$xmlD->load ( "../CFG/womcfg.xml" );
		
		$elem = $xmlD->documentElement;
		foreach ( $elem->childNodes as $item ) {
			switch ($item->nodeName) {
				case "dbtype" :
					$this->dbtype = $item->nodeValue;
					break;
				case "server" :
					$this->host = $item->nodeValue;
					break;
				case "port" :
					$this->port = $item->nodeValue;
					break;
				case "dbname" :
					$this->dbname = $item->nodeValue;
					break;
				case "user" :
					$this->user = $item->nodeValue;
					break;
				case "pass" :
					$this->password = $item->nodeValue;
					break;
				case "orasrvname" :
					$this->oraservice = $item->nodeValue;
					break;
				case "ext" :
					$this->ext = $item->nodeValue;
					break;
			}
		}
		switch ($this->dbtype) {
			case 0 :
				$cantidad = 1000;
				$this->conexion = pg_connect ( "host=$this->host port=$this->port dbname=$this->dbname user=$this->user password=$this->password" );
				// error_log(print_r("pg_connect('host=$this->host port=$this->port dbname=$this->dbname user=$this->user password=$this->password');", TRUE), 0);
				break;
			case 1 :
				// oci_connect($un, $pw, '//mymachine.mydomain:port/TNSNAME');//ORCL
				// $str = "user:'$this->user', pass:'$this->password', bd:$this->host:$this->port/$this->dbname";
				// error_log(print_r($str, TRUE), 0);
				// $this->conexion = oci_connect($this->user, $this->password, "$this->dbname");
				$cantidad = 1000;
				$this->conexion = oci_connect ( $this->user, $this->password, "$this->host:$this->port/$this->dbname" );
				//error_log ( print_r ( "oci_connect($this->user, $this->password, '$this->host:$this->port/$this->dbname');", TRUE ), 0 );
				break;
		}
	}
	public function ejecutar($query) {
		try {
			//error_log ( print_r ( $query, TRUE ), 0 );
			switch ($this->dbtype) {
				case 0 :
					$resul = pg_query ( $this->conexion, $query );
					break;
				case 1:
					/*
					 * $s = oci_parse($c, "create table tab1 (col1 number, col2 varchar2(30))");
					   oci_execute($s, OCI_DEFAULT);
					 */
					$resul = oci_parse ( $this->conexion, $query );
					if (! oci_execute ( $resul ))
						return false;
					break;
			}
		} catch ( Exception $ex ) {
			//error_log ( print_r ( "entra catch bdmanager $ex", TRUE ), 0 );
			$resul = false;
		}
		return ($resul);
	}
	public function ejecutarPSP($query,$arrParams)
	{
		try {
			switch ($this->dbtype){
				case 0:
					//$resul = pg_query($this->conexion, $query);
					$resul = pg_query_params($this->conexion, $query,$arrParams);
					//error_log(print_r($query, TRUE), 0);
					break;
				case 1:
					$resul = oci_parse($this->conexion, $query);
					//error_log(print_r($query, TRUE), 0);
					//error_log(print_r($arrParams, TRUE), 0);
					foreach ($arrParams as $clave => $valor) {
						oci_bind_by_name($resul, $clave, $arrParams[$clave]);
					}
					if (!oci_execute($resul))
						return false;
					break;
			}
		}
		catch (Exception $ex){
			//error_log(print_r("entra catch bdmanager $ex", TRUE), 0);
			$resul = false;
		}
		return ($resul);
	}
	public function desconectar() {
		switch ($this->dbtype) {
			case 0 :
				pg_close ( $this->conexion );
				break;
			case 1 :
				oci_close ( $this->conexion );
		}
	}
	public function __destruct() {
		switch ($this->dbtype) {
			case 0 :
				pg_close ( $this->conexion );
				break;
			case 1 :
				oci_close ( $this->conexion );
		}
	}
	
	// Esta funcion obtiene un objeto que contiene todos los registros obtebidos de una instruccion SELECT
	public function fetch($objResul) {
		switch ($this->dbtype) {
			case 0 :
				return pg_fetch_row ( $objResul );
				break;
			case 1 :
				// return oci_fetch_row($objResul);
				return oci_fetch_array ( $objResul, OCI_BOTH + OCI_RETURN_NULLS );
				break;
		}
	}
	
	// Esta funcion retorna el numero de registros obtenidos en una instruccion SELECT
	public function nrows($objResul) {
		switch ($this->dbtype) {
			case 0 :
				return pg_numrows ( $objResul );
				break;
			case 1 :
				// $this->fetch($objResul);
				return oci_num_rows ( $objResul );
				break;
		}
	}
	public function nrowsOra($objResul) {
		switch ($this->dbtype) {
			case 0 :
				return pg_numrows ( $objResul );
				break;
			case 1 :
				$this->fetch ( $objResul );
				return oci_num_rows ( $objResul );
		}
	}
	public function validate_token($token) {
		$exec_token = $this->ejecutar("select count(1) from web_usuario where token = '$token'");
		$num_exec_token = $this->fetch($exec_token);
		if($num_exec_token[0] > 0){
			return true;
		}else{
			return false;
		}
	}
	public function get_perfil($token) {
		$perfil = $this->ejecutar("select perfil from web_usuario where token = '$token'");
		$perfil = $this->fetch($perfil);
		if($perfil[0]){
			return $perfil[0];
		}else{
			return null;
		}
	}
	/*
	 * $query = "SELECT * FROM MVELNODE";
	 * $result = pg_exec($db, $query);
	 * echo "nUMERO DE LINEAS: " . pg_numrows($result);
	 *
	 * pg_close($db);
	 */
}

?>
