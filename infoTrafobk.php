<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Reportes</title>
<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
<script src="frameworks/js/jquery-2.1.1.js"></script>
<script>

	function CargarReporte(valor)
	{
		var xmlhttp;
        xmlhttp = new XMLHttpRequest();          
        xmlhttp.onreadystatechange=function()
         {          
        	
             if (xmlhttp.readyState==4 && xmlhttp.status==200)                
             {
            	 var res = xmlhttp.responseText;
            	 res = res.substring(0,res.length -1);
            	 res += "]";
            	 
            	 var titulos = valor.substring(7,valor.indexOf("FROM") - 1)
				var arr = JSON.parse(res);
			    var i;
			    var out = "<table>";
			    var Linea;
			    var x;
			    var titles = titulos.split(",");
			    out += "<tr>";
			    for(i = 0; i < titles.length; i++)
			    {
			    	Linea = titles[i];
			        out += "<th>";
			        out+= Linea;
			        out+="</th>";
			    }
			    out += "</tr>";
			    //document.getElementById("id01").innerHTML = out;
			    for(i = 0; i < arr.length; i++)
			    {
			        Linea = arr[i];
			        out += "<tr>"; 
			        for (x in Linea)
			        {
			            out += "<td>" +  Linea[x] + "</td>";                 
			        }
			        out+="</td>";
			    }
			    out += "</table>";
			    document.getElementById("id01").innerHTML = out;
             }
         }
         xmlhttp.open("GET","php/consulta2.php?strSQL=" +valor ,true);
         xmlhttp.send();
	}
	
	$( document ).ready(function() {
    	console.log( "ready!" );
    	CargarReporte("SELECT CODE, CUSTNAME  FROM CUSTMETR WHERE TPARENT='"+ document.getElementById("code").value +"'");
	});

</script>
<style>
h1 {
    border-bottom: 3px solid #cc9900;
    color: #996600;
    font-size: 30px;
}

table a:link {
	color: #666;
	font-weight: bold;
	text-decoration:none;
}
table a:visited {
	color: #999999;
	font-weight:bold;
	text-decoration:none;
}
table a:active,
table a:hover {
	color: #bd5a35;
	text-decoration:underline;
}
table {
	font-family:Arial, Helvetica, sans-serif;
	color:#666;
	font-size:12px;
	text-shadow: 1px 1px 0px #fff;
	background:#eaebec;
	margin:20px;
	border:#ccc 1px solid;

	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px;

	-moz-box-shadow: 0 1px 2px #d1d1d1;
	-webkit-box-shadow: 0 1px 2px #d1d1d1;
	box-shadow: 0 1px 2px #d1d1d1;
}
table th {
	padding:21px 25px 22px 25px;
	border-top:1px solid #fafafa;
	border-bottom:1px solid #e0e0e0;

	background: #ededed;
	background: -webkit-gradient(linear, left top, left bottom, from(#ededed), to(#ebebeb));
	background: -moz-linear-gradient(top,  #ededed,  #ebebeb);
}
table th:first-child {
	text-align: left;
	padding-left:20px;
}
table tr:first-child th:first-child {
	-moz-border-radius-topleft:3px;
	-webkit-border-top-left-radius:3px;
	border-top-left-radius:3px;
}
table tr:first-child th:last-child {
	-moz-border-radius-topright:3px;
	-webkit-border-top-right-radius:3px;
	border-top-right-radius:3px;
}
table tr {
	text-align: center;
	padding-left:20px;
}
table td:first-child {
	text-align: left;
	padding-left:20px;
	border-left: 0;
}
table td {
	padding:18px;
	border-top: 1px solid #ffffff;
	border-bottom:1px solid #e0e0e0;
	border-left: 1px solid #e0e0e0;

	background: #fafafa;
	background: -webkit-gradient(linear, left top, left bottom, from(#fbfbfb), to(#fafafa));
	background: -moz-linear-gradient(top,  #fbfbfb,  #fafafa);
}
table tr.even td {
	background: #f6f6f6;
	background: -webkit-gradient(linear, left top, left bottom, from(#f8f8f8), to(#f6f6f6));
	background: -moz-linear-gradient(top,  #f8f8f8,  #f6f6f6);
}
table tr:last-child td {
	border-bottom:0;
}
table tr:last-child td:first-child {
	-moz-border-radius-bottomleft:3px;
	-webkit-border-bottom-left-radius:3px;
	border-bottom-left-radius:3px;
}
table tr:last-child td:last-child {
	-moz-border-radius-bottomright:3px;
	-webkit-border-bottom-right-radius:3px;
	border-bottom-right-radius:3px;
}
table tr:hover td {
	background: #f2f2f2;
	background: -webkit-gradient(linear, left top, left bottom, from(#f2f2f2), to(#f0f0f0));
	background: -moz-linear-gradient(top,  #f2f2f2,  #f0f0f0);	
}





h2 { color: #666680; font-family: 'Montserrat', sans-serif; font-size: 22px; font-weight: 50; line-height: 24px; text-align: left; }
#reportes{
	/*background-color: red;*/
	float: left;
	overflow:hidden;
	width: 60%;
	height: 100px;
	padding-left: 20px;
	padding-top: 60px;
}
#id01{
	clear: both;
	margin-top: 0px;
	/*background-color: green;*/
}

select {
    padding:3px;
    margin: 0;
    -webkit-border-radius:4px;
    -moz-border-radius:4px;
    border-radius:4px;
    -webkit-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
    -moz-box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
    box-shadow: 0 3px 0 #ccc, 0 -1px #fff inset;
    background: #f8f8f8;
    color:#888;
    border:none;
    outline:none;
    display: inline-block;
    -webkit-appearance:none;
    -moz-appearance:none;
    appearance:none;
    cursor:pointer;
    width: 200px;
}
#logo{
	
	text-align: center;
	float:right;
	width: 25%;
	height: 150px;
	overflow:hidden;
	/*background-color: yellow;*/

}
img{
	with:80px;
	height: 100px;
	border-radius:5px;
	opacity: 0.7;
	

}
/*
a{
	text-decoration: none;
	font-family: 'Montserrat';
	font-size: 1.0em;
	padding: 1.5px;	
}
a:VISITED {
	color: #fff;
}
a:LINK {
	color: #fff;
}


#reportes{
	border-radius: 3px;
	background-color: #B2B2CC;
	color: #fff;
}

<a id="reportes" href="index.html">Principal</a>*/

</style>

</head>
<body>


<div id="reportes">
	<div class="combo">
		<h2>Informacion de transformador SPARD &reg;  </h2>
		<input id="code" type="hidden" value="<?php echo $_GET["trafo"]; ?>">
	</div >
	
</div>
<div id="logo"><img src="images/spard.jpg"></div>
<div id="id01"></div>
</body>
</html>