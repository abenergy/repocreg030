<?php
	require_once ('DBconnect.php');

	$cuenta = $_GET ["CUENTA"];
	$nodo = $_GET ["NODO"];
	$capacidad = $_GET ["CAP"];
	$descriptio = $_GET ["DESCRIPTIO"];
	$name = $_GET ["NOMBRE"];
	$tipo = $_GET ["TIPO"];
	$power = $_GET ["PF"];
	$email = $_GET ["EMAIL"];
	$media = $_GET ["MEDIA"];
	$max = -1;
	$MismoNodo = false;
	$MismoCorreo = false;
	$CodigoError = 0;
	$db = new BaseDatos ();

	$strSQL = "SELECT COUNT(ID) FROM GEN_SOLICITUDES WHERE ESTADO = 0 AND EMAIL = '$email' AND ELNODE = '$nodo'";
	$res = $db->ejecutar ($strSQL);

	while ( $row = $db->fetch ( $res )) 
	{
		$max = $row[0];
	}
	if($max != 0)
	{
		$MismoCorreo = true;
		$max = -1;
		$CodigoError = 1;
	}

	$strSQL = "SELECT COUNT(ID) FROM GEN_SOLICITUDES WHERE ESTADO = 0 AND ELNODE = '$nodo'";
	$res = $db->ejecutar ($strSQL);

	while ( $row = $db->fetch ( $res )) 
	{
		$max = $row[0];
	}
	if($max != 0)
	{
		$MismoNodo = true;
	}

	if($media == 0)//Validaciones tecnicas (en baja)
	{
		$strSQL = "SELECT PG, EG, EGV FROM TRANSFOR, LVELNODE WHERE TRANSFOR.CODE = LVELNODE.TPARENT AND LVELNODE.CODE = '$nodo'";
		$res = $db->ejecutar ($strSQL);

		while ( $row = $db->fetch ( $res )) 
		{
			$pg = $row[0];//Porcentaje de potencia generada
			$eg = $row[1];//Porcentaje de energia generada
			$egv = $row[2];//Porcentaje de energia generada fotovoltaicos
		}

		if($pg > 15)
		{
			$MismoCorreo = true;
			$max = -1;
			$CodigoError = 2;
		}
		if($eg > 50)
		{
			$MismoCorreo = true;
			$max = -1;
			$CodigoError = 3;
		}
	}
	else//Validaciones tecnicas (en media)
	{
		$strSQL = "SELECT PG, EG, EGV FROM FEEDERS, MVELNODE WHERE FEEDERS.CODE = MVELNODE.FPARENT AND MVELNODE.CODE = '$nodo'";
		$res = $db->ejecutar ($strSQL);

		while ( $row = $db->fetch ( $res )) 
		{
			$pg = $row[0];//Porcentaje de potencia generada
			$eg = $row[1];//Porcentaje de energia generada
			$egv = $row[2];//Porcentaje de energia generada fotovoltaicos
		}

		if($pg > 15)
		{
			$MismoCorreo = true;
			$max = -1;
			$CodigoError = 2;
		}
		if($eg > 50)
		{
			$MismoCorreo = true;
			$max = -1;
			$CodigoError = 3;
		}
	}

	if($MismoCorreo == false)//Se crea la solicitud
	{
		$strSQL = "INSERT INTO GEN_SOLICITUDES (CUENTA, ELNODE, CAPACIDAD, DESCRIPTIO, NAME, TIPO, POWERFACTOR, EMAIL, ESTADO, FECHA) VALUES ('$cuenta','$nodo',$capacidad,'$descriptio','$name','$tipo', '$power','$email','0',NOW())";
		$res = $db->ejecutar ($strSQL);

		$strSQL = "SELECT MAX(ID) FROM GEN_SOLICITUDES";
		$res = $db->ejecutar ( $strSQL );
		while ( $row = $db->fetch ( $res )) 
		{
			$max = $row[0];
		}

		$strSQL = "INSERT INTO GEN_EDIT (ID_SOL, FEC_OLD, FEC_NEW, EST_OLD, EST_NEW) VALUES ('$max',NOW(), NOW(),'0','0')";
		$res = $db->ejecutar ($strSQL);
	}
	else
	{
		$max = -1;
	}

 	$salida = array(
    	array("NumSolicitud",$max),
  		array("MismoNodo",$MismoNodo),
  		array("MismoCorreo",$MismoCorreo),
  		array("CodigoError",$CodigoError)
    );
	echo json_encode ($salida);
?>