<?php
require_once ('DBconnect.php');

$db = new BaseDatos ();

$strSQL = "select code, latitud, longitud, xsize, ysize from substati";
// error_log(print_r($strSQL, TRUE), 0);
$Substati = array ();
$res = $db->ejecutar ( $strSQL );
while ( $row = $db->fetch ( $res ) ) {
	$Substati [] = array (
			'code' => $row [0],
			'lat' => str_replace ( ",", ".", $row [1] ),
			'lon' => str_replace ( ",", ".", $row [2] ),
			'xsize' => $row [3],
			'ysize' => $row [4] 
	);
}
echo json_encode ( $Substati );
?>
