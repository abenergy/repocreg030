// Documentation: validatejs.org/ Git: github.com/ansman/validate.js
// form constraints
var constraints = {
    txtUserName: {
        presence: {
            message: "No puede estar vacío"
        },
        format: {
            pattern: /^[A-z ]+$/,
            message: "Ingrese letras únicamente"
        }
    },
    tipoSolicitud: {
        presence: true
    },
    txtCorreo: {
        presence: {
            message: "No puede estar vacío"
        },
        email: {
            message: "Correo email no valido"
        }
    },
    reTxtCorreo: {
        presence: {
            message: "No puede estar vacío"
        },
        equality: {
            attribute: "txtCorreo",
            message: "Los correos email no coinciden"
        }
    },
    fechaOperacion: {
        presence: {
            message: "No puede estar vacío"
        }
    },
    entrega_energia: {
        presence: true
    },
    txtObservaciones: {
        presence: {
            message: "No puede estar vacío"
        }
    },
    txtGenCap: {
        presence: {
            message: "No puede estar vacío"
        },
        numericality: {
            onlyInteger: false,
            greaterThan: 0,
            lessThanOrEqualTo: 5,
            notGreaterThan: "Debe ser mayor a 0",
            notLessThanOrEqualTo: "Debe ser menor o igual a 5"
        }
    },
    txtPowerFactor: {
        presence: {
            message: "No puede estar vacío"
        },
        numericality: {
            onlyInteger: false,
            greaterThan: 0.8,
            lessThanOrEqualTo: 1,
            notGreaterThan: "Debe ser mayor a 0.8",
            notLessThanOrEqualTo: "Debe ser menor o igual a 1"
        }
    },
    txtDistorcion: {
        presence: {
            message: "No puede estar vacío"
        },
        numericality: {
            onlyInteger: false,
            greaterThan: 0,
            lessThanOrEqualTo: 20,
            notGreaterThan: "Debe ser mayor a 0",
            notLessThanOrEqualTo: "Debe ser menor o igual a 20"
        }
    },
    txtTension: {
        presence: {
            message: "No puede estar vacío"
        },
        numericality: {
            onlyInteger: false,
            greaterThan: 100,
            lessThanOrEqualTo: 110,
            notGreaterThan: "Debe ser mayor a 100",
            notLessThanOrEqualTo: "Debe ser menor o igual a 101"
        }
    },
    txtEnergiaAnual: {
        presence: {
            message: "No puede estar vacío"
        },
        numericality: {
            onlyInteger: false,
            greaterThan: 0,
            notGreaterThan: "Debe ser mayor a 0"
        }
    },
    cmbTipo: {
        presence: true
    },
    txtCaracteristicas: {
        presence: {
            message: "No puede estar vacío"
        }
    },
    txtCuenta: {
        presence: {
            message: "No puede estar vacío"
        }
    },
    txtAlmacenamiento: {
        presence: true
    }
}

// form object
var form = document.querySelector("#solicitud_form");

// form onchange 
form.addEventListener("change", function(event) {
    var valid = formGroupValidation("#solicitud_form .group"+currentTab);
    disableNextBtn(valid);
});

// form onsubmit
form.addEventListener("submit", function(event) {
    event.preventDefault();
    var valid = formValidation("#solicitud_form .group"+currentTab);
    disableNextBtn(valid);
});

// form validation
function formValidation(formId) {
    var formGroup = document.querySelector(formId);
    var values = validate.collectFormValues(formGroup);
    var errors = validate(values, constraints, {fullMessages: false});
    var valid = false;

    if (!errors) valid = true;

    return valid;
}

// form group validation
function formGroupValidation(formId) {
    var values = validate.collectFormValues(form);
    var errors = validate(values, constraints, {fullMessages: false});
    var valid = true;

    var elems = form.querySelectorAll( formId+' input[name], '+formId+' select[name], '+formId+' textarea[name]');

    for (var i = 0; i < elems.length; i++) {
        if (errors[elems[i].name]) valid = false;
    }

    return valid;
}

function disableNextBtn(valid) {
    if (valid) {
        document.getElementById('nextBtn').classList.remove('disabled');
    } else {
        document.getElementById('nextBtn').classList.add('disabled');
    }
}

// Hook up the inputs to validate on the fly
var inputs = document.querySelectorAll("#solicitud_form input, #solicitud_form textarea, #solicitud_form select")
for (var i = 0; i < inputs.length; ++i) {
    inputs.item(i).addEventListener("change", function(event) {
        var errors = validate(form, constraints, {fullMessages: false}) || {};
        showErrorsForInput(this, errors[this.name])
    });
}

// input validation()
function inputValidation(elem) {
    var values = validate.collectFormValues(form);
    var errors = validate(values, constraints, {fullMessages: false});

    showErrorsForInput(elem, errors && errors[elem.name]);

    if (errors && errors[elem.name]) {
        elem.classList.remove('valid');
        elem.classList.add('invalid');
        document.getElementById('nextBtn').classList.add('disabled');
    } else {
        elem.classList.add('valid');
        elem.classList.remove('invalid');
    }
}

// Shows the errors for a specific input
function showErrorsForInput(input, errors) {
    var formGroup = closestParent(input.parentNode, "input-field");
    var messages = formGroup.querySelector(".helper-text");
    resetFormGroup(formGroup);

    if (errors) {
        input.classList.remove("valid");
        input.classList.add("invalid");
        _.each(errors, function(error) {
            addError(messages, error);
        });
    } else {
        input.classList.remove("invalid");
        input.classList.add("valid");
    }
}

/*/ Updates the inputs with the validation errors
function showErrors(form, errors, selector) {
    _.each(form.querySelectorAll(selector), function(input) {
        showErrorsForInput(input, errors && errors[input.name]);
    });
}
*/

// resetFormGroup()
function resetFormGroup(formGroup) {
    formGroup.classList.remove("has-error", "has-success");
    _.each(formGroup.querySelectorAll(".help-block.error"), function(el) {
        el.parentNode.removeChild(el);
    });
}

// Adds the specified error markup
function addError(messages, error) {
    var block = document.createElement("span");
    block.classList.add("help-block");
    block.classList.add("error");
    block.innerHTML = error;
    messages.appendChild(block);
}

function resetForm() {
    _.each(form.querySelectorAll("input[name], select[name]"), function(input) {
        var formGroup = closestParent(input.parentNode, "input-field");
        resetFormGroup(formGroup);
    });
}