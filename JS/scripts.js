var strTipoSolicitud = "solicitud";
var currentTab = 0;

function toggleClassById (elemId, className, remove) {
    var elem = document.getElementById(elemId);
    toggleClassByElem(elem, className, remove);
}

function toggleClassByElem (elem, className, remove) {
    var toggle = elem.classList.contains(className);

    if(remove == true){
        elem.classList.remove(className);
    } else {
        if (remove == false || toggle == false) elem.classList.add(className);
    }
}

function showLoader (loader) {
  if(loader == true){
    document.getElementById('loader').classList.remove('hidden');
  } else {
    document.getElementById('loader').classList.add('hidden');
  }
}

function hideParentEl (parentEl, className) {
    var hideEl = closestParent(parentEl, className);

    hideEl.classList.add('hidden');
}

// Recusively finds the closest parent that has the specified class
function closestParent(child, className) {
    if (!child || child == document) {
        return null;
    }
    if (child.classList.contains(className)) {
        return child;
    } else {
        return closestParent(child.parentNode, className);
    }
}

function toggleMessage (containerId, contentId, messageId, action) {
    var content = document.querySelectorAll('#'+containerId+' .'+contentId);
    var message = document.querySelectorAll('#'+containerId+' .'+messageId);

    if(action == 'remove'){
        content[0].classList.add('hideall');
        message[0].classList.remove('hideall');
    } else {
        content[0].classList.remove('hideall');
        message[0].classList.add('hideall');
    }
}

// Formulario de Solicitud
// w3schools.com/howto/howto_js_form_steps.asp?
function nextPrev(n) {
    var x = document.getElementsByClassName("tab");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }

    // next tab..
    currentTab = currentTab + n;

    if (formGroupValidation("#solicitud_form .group"+currentTab) == false) {
        document.getElementById('nextBtn').classList.add('disabled');
    }
    
    if (currentTab == 1) tipoSolicitudInit(strTipoSolicitud);
    if (currentTab == 4) RealizarSimulacion();
    if (currentTab == 5) EnviarSolicitud();

    if (currentTab >= x.length) {
        closeModal('frmSolicitud');
        resetTab();
        return false;
    }

    if (n == -1) document.getElementById('nextBtn').classList.remove('disabled');

    showTab(currentTab);
}

function showTab(tab) {
    var x = document.getElementsByClassName("tab");
    var buttonText = "Siguiente";
    var firstPage = false;
    var lastPage = false;
    var firstLastPage = false;

    if (tab == 0) { buttonText = "Solicitud"; toggleClassById("nextBtn", "modal-close", true); firstPage = true; firstLastPage = true; }
    if (tab == 3) buttonText = "Simular";
    if (tab == 4) buttonText = "Enviar";
    if (tab == 5) buttonText = "Cerrar";
    if (tab == (x.length - 1)) { lastPage = true; firstLastPage = true; }
    
    document.getElementById("nextBtn").innerHTML = buttonText;
    toggleClassById("prevBtn", "hidden", !firstLastPage);
    toggleClassById('btnRegistro', 'hidden', firstPage);
    toggleClassById("nextBtn", "info", !firstPage);
    x[tab].style.display = "block";
}

function hideTab(n) {
    var x = document.getElementsByClassName("tab");
    x[n].style.display = "none";
}

function resetTab() {
    var x = document.getElementsByClassName("tab");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    
    currentTab = 0;
    showTab(currentTab);
}

resetTab();

function validarFactor (msg) {
    if (!isNaN(parseFloat(msg)) && isFinite(msg)) {
        var obj = document.getElementById("t_sistema");
        obj.classList.add('valid');
        
        if(msg <= 0.1){
            obj.value = "Generador de Distribución (≤ 0.1 MW)"
        } else if (msg > 0.1 && msg <= 1) {
            obj.value = "Autogenerador a Pequeña Escala (> 0.1 MW ≤ 1 MW)"
        } else {
            obj.value = "Autogenerador a Gran Escala (> 1 MW ≤ 5 MW)"
        }
    } else {
        obj.value = "N/A"
    }

    updateForm();
}

//Make the DIV element draggagle:
function dragElement(elmnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  if (document.getElementById(elmnt.id + "_draggable")) {
    document.getElementById(elmnt.id + "_draggable").onmousedown = dragMouseDown;
  } else {
    elmnt.onmousedown = dragMouseDown;
  }

  function dragMouseDown(e) {
    e = e || window.event;
    pos3 = e.clientX;
    pos4 = e.clientY;

    document.onmouseup = closeDragElement;
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {
    document.onmouseup = null;
    document.onmousemove = null;
  }
}

dragElement(document.getElementById('openLayer'));

function enableBtn(){
    document.getElementById('continue').classList.remove('disabled');
};

function continueBtn () {
    document.body.classList.add("loggued");

    showFeature();

    var imported = document.createElement('script');
    var key = "AIzaSyAAeJz01lGsGFJIOa8CgsmKhy-L-AXjjao";

    imported.src = 'https://maps.googleapis.com/maps/api/js?key='+ key +'&libraries=places&callback=Init';
    document.head.appendChild(imported);
}

function frmSolicitudInit(value) {
    openModal('frmSolicitud');
    updateForm();

    if (currentTab == 0) toggleClassById('nextBtn', 'disabled', true);
    
    if (value < 100) value = 110
    updateConstraint('txtTension', {presence: {message: 'No puede estar vacío'}, numericality: { onlyInteger: false, greaterThan: 100, lessThanOrEqualTo: value, notGreaterThan: 'Debe ser mayor a 100', notLessThanOrEqualTo: 'Debe ser menor o igual a '+value}});

}

function updateConstraint (item, value, action) {
    if (action == 'delete') {
        delete constraints[item];
    } else {
        constraints[item] = value;
    }
}

function tipoConexion(option) {
    var limit = 5;
    var unhide = false;
    var updateAction = 'delete';
    
    if (option == "1") {
        unhide = true;
        updateAction = '';
    }

    if (option == "2") limit = 0.1;
    
    updateConstraint('txtGenCap', {presence: {message: 'No puede estar vacío'}, numericality: {onlyInteger: false, greaterThan: 0, lessThanOrEqualTo: limit, notGreaterThan: "Debe ser mayor a 0", notLessThanOrEqualTo: "Debe ser menor o igual a "+limit}}); 
    updateConstraint('elementosLimitan', {presence: {message: 'No puede estar vacío'}}, updateAction);
    toggleClassById('elemLimit', 'hideall', unhide);
    
    var input = document.getElementById("txtGenCap");
    var errors = validate(form, constraints, {fullMessages: false});
    if (input.value != "") showErrorsForInput(input, errors['txtGenCap']);
}

function tipoSolicitudInit(option) {
    var elem = document.getElementById('fechaOperacion');
    var instance = M.Datepicker.getInstance(elem);

    switch(option) {
        case "registro":
            var date = new Date();
            instance.options.minDate = "";
            instance.options.maxDate = date;
            
            instance.setDate(date);
            instance.setInputValue(date);
            instance.gotoDate(date);
            break;

        case "solicitud":
            var min = new Date();
            var max = new Date();
            min.setDate(min.getDate() + 12);
            max.setDate(max.getDate() + 12);
            max.setMonth(max.getMonth() + 6);

            instance.options.minDate = min;
            instance.options.maxDate = max;

            var dateMin = new Date();
            dateMin.setDate(dateMin.getDate() + 13);
            
            instance.setDate(dateMin);
            instance.setInputValue(dateMin);
            instance.gotoDate(dateMin);
            break;
    }
}

// habilitar para deshabilitar el login -desarrollo-
// continueBtn();