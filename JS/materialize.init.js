document.addEventListener('DOMContentLoaded', function() {
  var elems = document.querySelectorAll('.fixed-action-btn');
  var instances = M.FloatingActionButton.init(elems, {
    direction: 'top'
  });
});

document.addEventListener('DOMContentLoaded', function() {
  var elems = document.querySelectorAll('.modal');
  var instances = M.Modal.init(elems);
});

document.addEventListener('DOMContentLoaded', function() {
  var elems = document.querySelectorAll('.sidenav');
  var instances = M.Sidenav.init(elems);
});

document.addEventListener('DOMContentLoaded', function() {
  var elems = document.querySelectorAll('.autocomplete');
  var instances = M.Autocomplete.init(elems);
});

document.addEventListener('DOMContentLoaded', function() {
  var elems = document.querySelectorAll('select');
  var instances = M.FormSelect.init(elems);
});

document.addEventListener('DOMContentLoaded', function() {
  var date = new Date();
  var elems = document.querySelectorAll('.datepicker');
  var instances = M.Datepicker.init(elems, {
    autoClose: true,
    i18n: {
      cancel:'Cancelar',
      close:'Cerrar',
      done:'Aceptar',
      months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      monthsShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
      weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
      weekdaysAbbrev:	['D','L','M','Mi','J','V','S']
    },
    selectMonths: true,
    selectYears: true,
    maxDate: date,
    onSelect: function(date) {
        this.setInputValue(date);
        this.close();
    }
  });
});

document.addEventListener('DOMContentLoaded', function() {
  var elems = document.querySelectorAll('.tooltipped');
  var instances = M.Tooltip.init(elems);
});

document.addEventListener('DOMContentLoaded', function() {
  var elems = document.querySelectorAll('.collapsible');
  var instances = M.Collapsible.init(elems);
});

document.addEventListener('DOMContentLoaded', function() {
  var elems = document.querySelectorAll('.tabs');
  var instance = M.Tabs.init(elems, {swipeable:true});
});


               /*|                    / _|                | | (_)                
  ___ _   _ ___| |_ ___  _ __ ___    | |_ _   _ _ __   ___| |_ _  ___  _ __  ___ 
 / __| | | / __| __/ _ \| '_ ` _ \   |  _| | | | '_ \ / __| __| |/ _ \| '_ \/ __|
| (__| |_| \__ \ || (_) | | | | | |  | | | |_| | | | | (__| |_| | (_) | | | \__ \
 \___|\__,_|___/\__\___/|_| |_| |_|  |_|  \__,_|_| |_|\___|\__|_|\___/|_| |_|__*/


 function showLoader (loader) {
  if(loader == true){
    document.getElementById('loader').classList.remove('hidden');
  } else {
    document.getElementById('loader').classList.add('hidden');
  }
}

function updateForm() {
    var elems = document.querySelectorAll('select');

    M.FormSelect.init(elems);
    M.updateTextFields();
}

function openModal(id) {
    var elem = document.getElementById(id);
    var instance = M.Modal.getInstance(elem);

    instance.open();
    updateForm();
}

function closeModal(id) {
    var elem = document.getElementById(id);
    var instance = M.Modal.getInstance(elem);

    instance.close();
}

function toggleModal(id, toggle) {
    var elem = document.getElementById(id);
    var instance = M.Modal.getInstance(elem);

    if (toggle == 'close') instance.close(); else instance.open();
}

function closeSidenav () {
    var elem = document.getElementById('slide-out');
    var instance = M.Sidenav.getInstance(elem);
    instance.close();
}

function openAlert(message) {
    M.toast({html: message})
}

function openTooltips() {
    var elems = document.querySelectorAll('.tooltipped');
    var instances = M.Tooltip.init(elems);

    if (instances) instances.open();
}

function showFeature() {
  var elems = document.querySelectorAll('.tap-target')[0];
  var instances = M.TapTarget.init(elems);

  instances.open();
}