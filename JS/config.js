	function Selected(i){
    		var mapDark = document.getElementById('dark');
    		var mapWhite = document.getElementById('white');
    		var mapNormal = document.getElementById('normal');
            var xmlhttp;
            xmlhttp = new XMLHttpRequest();          
            var strSQL = "DELETE FROM ADMON_VISOR WHERE CODE='MAP_STYE'"
            xmlhttp.open("GET","php/ejecutar.php?sql="+strSQL ,false);
            xmlhttp.send();
            //1 normal style else..
            //2 dark theme style
            //3 white theme style           
    		if (i==1) {
    			
                mapNormal.style.border='2px solid #3399ff';
                mapWhite.style.border='none';
                mapDark.style.border='none';
                /*modifica en bd con el valor del estilo seleccionado*/
                strSQL = "INSERT INTO ADMON_VISOR(CODE,VALOR) VALUES('MAP_STYE','1')"

                
    		}
    		else if(i==2){
    			mapDark.style.border='2px solid #3399ff';
                mapWhite.style.border='none';
                mapNormal.style.border='none';
                /*modifica en bd con el valor del estilo seleccionado*/
                strSQL = "INSERT INTO ADMON_VISOR(CODE,VALOR) VALUES('MAP_STYE','2')"
    		}
    		else if(i==3){
                mapWhite.style.border='2px solid #3399ff';
                mapDark.style.border='none';
                mapNormal.style.border='none';
                /*modifica en bd con el valor del estilo seleccionado*/
                strSQL = "INSERT INTO ADMON_VISOR(CODE,VALOR) VALUES('MAP_STYE','3')"
    		}
            xmlhttp.open("GET","php/ejecutar.php?sql="+strSQL ,false);
            xmlhttp.send();           
            if(xmlhttp.responseText!="0" && xmlhttp.responseText!="")
            {
                //CAMBIO REALIZADO...
            }
    	}
      
        function guardarConfi()
        {
            var intSubestacion=$('input[id=subestacion]').val();
            var intLineaMedia=$('input[id=lin-media]').val();
            var intLineaBaja=$('input[id=lin-baja]').val();
            var intPosteMEdia=$('input[id=poste-media]').val();
            var intPosteBaja=$('input[id=poste-baja]').val();
            var intTransformador=$('input[id=trafo]').val();
            var intCliente=$('input[id=usuario]').val();
            var intBancoUsuario=$('input[id=bancoUsuario]').val();
            var intSeccionador=$('input[id=seccionador]').val();
            var intReconectador=$('input[id=reconectador]').val();
            var intLuminaria=$('input[id=luminaria]').val();

            //PREPARA LA TABLA PARA LA INSERCION...
            ejecutarSQL("DELETE FROM ADMON_VISOR WHERE CODE!='MAP_STYE'");
            //inserta los valores
            ejecutarSQL("INSERT INTO ADMON_VISOR(CODE,VALOR) VALUES('SIZE_SUB','" + intSubestacion + "')");
            ejecutarSQL("INSERT INTO ADMON_VISOR(CODE,VALOR) VALUES('SIZE_LN_MEDIA','" + intLineaMedia + "')");
            ejecutarSQL("INSERT INTO ADMON_VISOR(CODE,VALOR) VALUES('SIZE_LN_BAJA','" + intLineaBaja + "')");
            ejecutarSQL("INSERT INTO ADMON_VISOR(CODE,VALOR) VALUES('SIZE_NF_MEDIA','" + intPosteMEdia + "')");
            ejecutarSQL("INSERT INTO ADMON_VISOR(CODE,VALOR) VALUES('SIZE_NF_BAJA','" + intPosteBaja + "')");
            ejecutarSQL("INSERT INTO ADMON_VISOR(CODE,VALOR) VALUES('SIZE_TRAFO','" +intTransformador + "')");
            ejecutarSQL("INSERT INTO ADMON_VISOR(CODE,VALOR) VALUES('SIZE_CUST','" +intCliente + "')");
            ejecutarSQL("INSERT INTO ADMON_VISOR(CODE,VALOR) VALUES('SIZE_CMBANK','" + intBancoUsuario+ "')");
            ejecutarSQL("INSERT INTO ADMON_VISOR(CODE,VALOR) VALUES('SIZE_SWITCH','" + intSeccionador+ "')");
            ejecutarSQL("INSERT INTO ADMON_VISOR(CODE,VALOR) VALUES('SIZE_RECLOSER','" + intReconectador+ "')");
            ejecutarSQL("INSERT INTO ADMON_VISOR(CODE,VALOR) VALUES('SIZE_STRLIGHT','" + intLuminaria+ "')");
            
            var config = document.getElementById("globalconfig");
            config.style.visibility = 'hidden';
            var mapatematicost = document.getElementById("frmThematicMap");
            mapatematicost.style.visibility = 'hidden';

            var btnMapa = document.getElementById("boton-layers");
            btnMapa.style.border='none';
            initialize();        

            
        }
        function ejecutarSQL(sql){
            var xmlhttp;
            xmlhttp = new XMLHttpRequest();          
            xmlhttp.open("GET","php/ejecutar.php?sql="+sql ,false);
            xmlhttp.send();
        }

        $( document ).ready(function() {
            //logica para cargar la configuracion de base de datos...
               var xmlhttp;
               xmlhttp = new XMLHttpRequest();          
               var mapDark = document.getElementById('dark');
               var mapWhite = document.getElementById('white');
               var mapNormal = document.getElementById('normal');
               xmlhttp.onreadystatechange=function()
                {               
                    if (xmlhttp.readyState==4 && xmlhttp.status==200)                
                    {                                        
                        var configuracion = xmlhttp.responseText;
                        configuracion = JSON.parse(configuracion);
                        var mapstyle ;
                        //carga la configuracion
                        if(configuracion != null || configuracion != ""){

                            for (i=0;i<configuracion.length;i++){
                               //document.write(a[x] + " ");
                            
                            
                                if(configuracion[i].CODE == "MAP_STYE"){
                                    mapstyle= configuracion[i].VALOR;

                                }
                                if(configuracion[i].CODE == "SIZE_SUB"){
                                    document.getElementById("subestacion").value= configuracion[i].VALOR;
                                }
                                if(configuracion[i].CODE == "SIZE_LN_MEDIA"){
                                    document.getElementById("lin-media").value= configuracion[i].VALOR;                            
                                }
                                if(configuracion[i].CODE == "SIZE_LN_BAJA"){
                                    document.getElementById("lin-baja").value= configuracion[i].VALOR;
                                }
                                if(configuracion[i].CODE == "SIZE_NF_MEDIA"){
                                    document.getElementById("poste-media").value= configuracion[i].VALOR;
                                }
                                if(configuracion[i].CODE == "SIZE_NF_BAJA"){
                                    document.getElementById("poste-baja").value= configuracion[i].VALOR;
                                }
                                if(configuracion[i].CODE == "SIZE_TRAFO"){
                                    document.getElementById("trafo").value= configuracion[i].VALOR;
                                }
                                if(configuracion[i].CODE == "SIZE_CUST"){
                                    document.getElementById("usuario").value= configuracion[i].VALOR;
                                }
                                if(configuracion[i].CODE == "SIZE_CMBANK"){
                                    document.getElementById("bancoUsuario").value= configuracion[i].VALOR;
                                }
                                if(configuracion[i].CODE == "SIZE_SWITCH"){
                                    document.getElementById("seccionador").value= configuracion[i].VALOR;
                                }
                                if(configuracion[i].CODE == "SIZE_STRLIGHT"){
                                    document.getElementById("luminaria").value= configuracion[i].VALOR;
                                }

                                                               
                            }
                            if(mapstyle == "1"){
                                mapNormal.style.border='2px solid #3399ff';
                                mapWhite.style.border='none';
                                mapDark.style.border='none';

                            }
                            else if(mapstyle == "2"){
                                mapDark.style.border='2px solid #3399ff';
                                mapWhite.style.border='none';
                                mapNormal.style.border='none';
                            }
                            else if(mapstyle == "3"){
                                mapWhite.style.border='2px solid #3399ff';
                                mapDark.style.border='none';
                                mapNormal.style.border='none';
                            }
                            

                        }                   
                    }
                }            
                xmlhttp.open("GET","php/CargaConfig.php",true);
                xmlhttp.send();
        });
