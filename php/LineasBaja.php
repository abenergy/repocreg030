<?php
require_once ('DBconnect.php');

$Parents = $_GET ["Parents"];
$ParentsArray = array ();
$ParentsArray = preg_split ( "/[\s|]+/", $Parents );
$pCode = "";
if (count ( $ParentsArray ) == 1)
{
    $Tparent = $ParentsArray [0];
    $Fparent = "";
}
if (count ( $ParentsArray ) > 1)
{
    $Tparent = $ParentsArray [0];
    $Fparent = $ParentsArray [1];
}
$db = new BaseDatos ();

$strSQL = "select code, lat1, lon1, lat2, lon2 from lvlinsec where aco = 0 and tparent = '$Tparent'";

$Lineas = array ();
$res = $db->ejecutar ( $strSQL );
while ( $row = $db->fetch ( $res ) ) {
	$Lineas [] = array (
			'code' => $row [0],
			'lat1' => str_replace ( ",", ".", $row [1] ),
			'lon1' => str_replace ( ",", ".", $row [2] ),
			'lat2' => str_replace ( ",", ".", $row [3] ),
			'lon2' => str_replace ( ",", ".", $row [4] )
	);
}

echo json_encode ( $Lineas );
?>
