/**
 * 
 */
function guardarMantenimiento()
{
	var manid = document.getElementById("txtMantIdMant").value;
	var ot = document.getElementById("txtMantIdOT").value;
	var codel = document.getElementById("txtMantCodEl").value;
	var tipoel = document.getElementById("txtMantTipoEl").value;
	var fecha = document.getElementById("mantFecha").value;
	var estado = document.getElementById("mantEstado").value;
	var info = document.getElementById("txtmantInfo").value;
    
	var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET","php/addmantenimiento.php?id="+manid+"&otid="+ot+"&codel="+codel+"&typel="+tipoel+"&date="+fecha+"&state="+estado+"&comm="+info,false);
    xmlhttp.send();
    
    var lightbox = document.getElementById("frmMantenimiento");
	lightbox.style.visibility = 'hidden';
}

function cerrarMant()
{
	var lightbox = document.getElementById("frmMantenimiento");
	lightbox.style.visibility = 'hidden';
	reiniciarDialMant();
}

function reiniciarDialMant(){
	document.getElementById("txtMantIdMant").value = "";
	document.getElementById("txtMantIdOT").value = "";
	document.getElementById("txtMantCodEl").value = "";
	document.getElementById("txtMantTipoEl").value = "";
	document.getElementById("mantFecha").value = "";
	document.getElementById("mantEstado").value = null;
	document.getElementById("txtmantInfo").value = "";
}