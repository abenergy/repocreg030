<?php
require_once ('DBconnect.php');

$Parents = $_GET ["Trafo"];
$ParentsArray = array ();
$ParentsArray = preg_split ( "/[\s|]+/", $Parents );
$pcode = "";
if (count ( $ParentsArray ) == 1) {
    $Tparent = $ParentsArray [0];
    $Fparent = "";
}
if (count ( $ParentsArray ) > 1) {
    $Tparent = $ParentsArray [0];
    $Fparent = $ParentsArray [1];
}
if ($Tparent == "TODOS"){
    $token = $Fparent;
    $Fparent = "";
}
//error_log(print_r($ParentsArray, TRUE), 0);
$db = new BaseDatos ();
$strSQL = "select MVELNODE.code AS code, mvphnode.latitud as latitud, mvphnode.longitud as longitud, pg, eg from mvphnode, MVELNODE, feeders WHERE FEEDERS.CODE = MVELNODE.FPARENT AND MVPHNODE.CODE = MVELNODE.PHNODE AND fparent = (SELECT FPARENT FROM TRANSFOR WHERE CODE = '$Parents')";

$Postes = array ();
$res = $db->ejecutar ( $strSQL );
while ( $row = $db->fetch ( $res ) ) {
	$Postes [] = array (
			'code' => $row [0],
			'lat1' => str_replace ( ",", ".", $row [1] ),
			'lon1' => str_replace ( ",", ".", $row [2] ),
			'pg' => str_replace ( ",", ".", $row [3] ),
			'eg' => str_replace ( ",", ".", $row [4] )
	);
}
//$db->desconectar ();
echo json_encode ( $Postes );
?>