<?php
	require_once ('DBconnect.php');

	$nodo = $_GET ["NODO"];
	$capacidad = $_GET ["CAP"];
	$power = $_GET ["PF"];
	$db = new BaseDatos ();

	$mvabase = 1;
	//substr_replace($power, ".", ",");
	$strSQL = "select rthev, xthev, e_kv, f_kv, kvnom from lvelndrs, LVELNODE where lvelndrs.srccode = LVELNODE.code AND LVELNODE.CODE = '$nodo'";
	$res = $db->ejecutar ($strSQL);
	while ($row = $db->fetch ($res)) 
	{
		$rthev = $row[0];
		$xthev = $row[1];
		$e_kv  = $row[2];
		$f_kv  = $row[3];
		$kVNom = $row[4];
	}

	$sinfi = sin(acos($power));
	$Dp = -$capacidad * $power / ( $mvabase);
	$Dq = -$capacidad * $sinfi / ( $mvabase);

	$a = 1 - $rthev * $Dp - $xthev * $Dq;
	$b =   - $rthev * $Dq - $xthev * $Dp;
	$c =   - $xthev * $Dp + $rthev * $Dq; 
	$d = 1 - $xthev * $Dq + $rthev * $Dp;
	$g =   - $rthev * $Dp - $xthev * $Dq;
	$h =   - $xthev * $Dp + $rthev * $Dq;

	$De = $Df = 0.0;
	$Det = $a * $d - $c * $b;

	if($Det != 0.0) 
	{
		$De = ($g * $d - $h * $b) / $Det;
		$Df = ($a * $h - $g * $c) / $Det;
	}

	$eNew = $e_kv + $De;
	$fNew = $f_kv + $Df;

	$kVmagNew = sqrt($eNew*$eNew + $fNew*$fNew);

    $currMagkV = sqrt($e_kv*$e_kv + $f_kv*$f_kv);

	$RegOld = 100.0 * (1.0 - $currMagkV) / $currMagkV;
	$DropOld = 100.0 * (1.0 - $currMagkV);

    $salida = array(
    	array("P",($Dp * $mvabase)),
  		array("Q",($Dq * $mvabase)),
  		array("eNew",$eNew),
  		array("fNew",$fNew),
  		array("e_KV",$e_kv),
  		array("f_KV",$f_kv),
  		array("kVNOM",$kVNom),
  		array("currentKV",($currMagkV * $kVNom)),  		
  		array("NewkV",($kVmagNew * $kVNom)),
  		array("Reg",(100.0 * (1.0 - $kVmagNew) / $kVmagNew)),
  		array("Drop",(100.0 * (1.0 - $kVmagNew))),
  		array("RegOld",$RegOld),
  		array("DropOld",$DropOld)
    );
				
	echo json_encode ($salida);
?>